#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(X) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),X)

#define COUT_COLOR(X, Y) {SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Y); \
std::cout << X << std:: endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);}

#define GREEN	10
#define CYAN	11
#define RED		12
#define MAGENTA	13
#define YELLOW	14
#define WHITE	15

using namespace std;

#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <set>
#include <map>
#include<unordered_map>
#include <algorithm>

/*
* STL : STANDARD TEMPLATE LIBRARIE
*
* Ensemble d'outils disponibles sur toutes les plateformes C++ (windows, linux...)
*
* - containers : objects that handle a collection of others objects (vector, list, set, map, pair, stack, queue....)
* - iterators : permettent de parcourir les containers
* - Algorithms : search, sort, modify, count... ( <algorithm> )
* - Functors (Function objects dans <functional> ) : classe surchargeant l'op�rateur "()" de sorte � pouvoir �tre utilis�es comme des fonctions https://youtu.be/qcuYSqHxqLk
*
* https://www.youtube.com/watch?v=BKBXM7ypQG0&list=PL1w8k37X_6L9NXrP1D31hDTKcdAPIL0cG
*
* https://www.youtube.com/playlist?list=PLZ9NgFYEMxp5oH3mrr4IlFBn03rjS-gN1
*/


/*
* CONTAINERS : https://en.cppreference.com/w/cpp/container
*
* - sequence : vector, stack, queue, list, forward_list, array, deque
* - associative
*	- ordonn� : set, map, multiset, multimap
*	- non ordonn� : unordered_set, unordered_map, unordered_multiset, unordered_multimap
*
* SEQUENCE VS ASSOCIATIVE : https://www.geeksforgeeks.org/sequence-vs-associative-containers-cpp/
*
* SEQUENCE
* - vector : dynamic contiguous array
* - array : static contiguous array
* - deque : double-ended queue
* - forward_list : singly-linked list
* - list : doubly-linked list
*
* ASSOCIATIVE
* - set : collection of unique keys, sorted by keys
* - map : collection of key-value pairs, sorted by keys, keys are unique
* - multiset : collection of keys, sorted by keys
* - multimap : collection of key-value pairs, sorted by keys
*
* - unordered_set : collection of unique keys, hashed by keys
* - unordered_map : collection of key-value pairs, hashed by keys, keys are unique
* - unordered_multiset : collection of keys, hashed by keys
* - unordered_multimap : collection of key-value pairs, hashed by keys
*
*
* CONTAINER ADAPTERS : Interfaces that implement special functionality on top of Sequence containers
*
* - stack : adapts a container to provide stack (LIFO data structure)
* - queue : adapts a container to provide queue (FIFO data structure)
* - priority_queue : adapts a container to provide priority queue
*/

/*
* ITERATORS
* - Objects that enable traversal of containers in some order, for either reading or writing
* - Iterators are defined as templates and must comply a very specific set of rules in order to qualify as one of many types of iterators
*
* Are pointers and iterators the same thing ?
* - no
* - Pointers are variable that can be indirected to refer to a memory location
* - Iterator is a higher abstraction for elements used to travel containers,
* whereas pointers and integers are special types of iterators, not suitable for every container
*
* TYPES OF ITERATORS :
* - input iterator :
*	- Only able to read (only accessing, not assigning)
*	- only moves forward
*	- can be incremented (it++, ++it,  BUT NOT --it, it--)
*	- only one pass possible
*	- least requirement supported by STL
*	- suitable for input streams, such as keyboard buffers or read-only files
* - output iterator :
* *	- Only able to write (only assigning, not accessing)
*	- only moves forward
*	- can be incremented (it++, ++it,  BUT NOT --it, it--)
*	- only one pass possible
*	- least requirement supported by STL
*	- suitable for output streams, such as sreen text or write-only files
* - forward iterator :
*	- It has requirements of both the input and the output iterator
*	- Able to read and write
*	- Only moves forward
*	- Supports multiple path of containers
*	- Suitable for traversing Singly linked list
* - bidiretional iterator :
*	- All capabilities of Forward iterators + Backward traversal
*	- Suitable for doubly linked list
* - random-access iterator :
*	- All capabilities of Bidirectional iterator + random access by means of indexing
*	- Suitable for vectors ans similar containers (e.g arrays)
*
* ITERATORS FUNCTIONS :
* - begin() : returns iterator to the begining of the container
* - end() :
* 	- returns an iterator to the element following the last element of the container
* 	- acts as a placeholder, attempting to access it results in undefined behavior
* - rbegin() (reverse begin): returns iterator to the last element of the container
* - rend() (reverse end) : returns a reverse iterator pointing to the theoritical element right before the first element in the array container
*/


/*
* Un it�rateur est un objet permettant de parcourir une s�quence d'�l�ments,
* telle qu'un tableau, une liste, ou tout autre conteneur.
* Les it�rateurs offrent une interface uniforme pour acc�der et manipuler les �l�ments d'une s�quence,
* ind�pendamment de la fa�on dont la s�quence est impl�ment�e.
*
* Les it�rateurs sont utilis�s dans de nombreuses parties du langage C++,
* en particulier avec la biblioth�que standard (STL, Standard Template Library).
*
* Ils facilitent la conception de code g�n�rique,
* permettant aux algorithmes de travailler avec diff�rents types de conteneurs de mani�re coh�rente.
*
* Exemples de types d'it�rateurs  :
*
*  It�rateurs de d�but et de fin :
*
* - begin() : Renvoie un it�rateur pointant vers le premier �l�ment de la s�quence.
* - end() : Renvoie un it�rateur pointant un-past-the-end de la s�quence.
*
* It�rateurs d'insertion et de suppression :
*
* - insert() : Ins�re un �l�ment � la position sp�cifi�e.
* - erase() : Supprime un �l�ment � la position sp�cifi�e.
*
* It�rateurs de d�placement :
*
* - advance() : D�place l'it�rateur d'un certain nombre d'�l�ments.
*
* It�rateurs de recherche :
*
* - find() : Recherche un �l�ment dans la s�quence.
*/

// ATTENTION : n�cessite c++ 17 ou ult�rieur (c++ 17 => (project > Properties > C/C++ > Language > C++ Language Standard)
template <template <typename> class CollectionType, typename ItemType>
void print_container(CollectionType<ItemType>& collection)
{
	for (auto item : collection)
		cout << item << " ";
	cout << endl;
}

int main()
{
	SetConsoleOutputCP(1252);

	// -----------------------------------------------------------
	// ------------------------- VECTOR --------------------------
	// -----------------------------------------------------------
	// 
	// https://youtu.be/0DNDvuHZDiQ
	COUT("========= STL - vector ==========");
	vector<int> v = { 1, 2, 3, 9, 8, 7 };

	// Parcours 

	cout << "\nParcours du vecteur avec boucle for traditionnelle : ";

	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	cout << "\nParcours du vecteur avec boucle for type foreach : ";
	for (int vv : v)
	{
		cout << vv << " ";
	}
	cout << endl;

	// Appel explicite � l'it�rateur
	cout << "\nParcours du vecteur via appel explicite � it�rateur : ";

	vector<int>::iterator it; // d�claration d'un it�rateur

	// begin() : Returns an iterator pointing to the first element in the sequence
	// end() : Returns an iterator pointing to the last element in the sequence
	for (it = v.begin(); it != v.end(); it++)
	{
		cout << *it << " ";
	}
	COUT("");
	COUT("");

	COUT("v.size() = " << v.size() << " - v.capacity() = " << v.capacity());

	v.push_back(4);
	v.push_back(5);

	print_container<vector, int>(v); // 1 2 3 9 8 7 4 5

	COUT("v.size() = " << v.size() << " - v.capacity() = " << v.capacity());

	/*cout << "v[2] = " << v[2] << endl;
	cout << " v.at(2) = " << v.at(2) << endl;*/

	v.pop_back();

	print_container<vector, int>(v); // 1 2 3 9 8 7 4

	COUT("v.size() = " << v.size() << " - v.capacity() = " << v.capacity());

	v.erase(v.begin() + 1, v.begin() + 3); // supprime les �l�ments d'indice 1 � 3 (exclu)

	print_container<vector, int>(v); // 1 9 8 7 4

	COUT("v.size() = " << v.size() << " - v.capacity() = " << v.capacity());

	COUT("*v.begin() : " << *v.begin()); // 1

	COUT("*v.rbegin() : " << *v.rbegin()); // 4

	auto ite = v.begin() + 1; // pointe sur le second �l�ment (9)

	COUT("*ite = " << *ite); // 9

	ite = v.insert(ite, 100); // ite = => pour ne pas invalider l'it�rateur

	print_container<vector, int>(v); // 1 100 9 8 7 4

	COUT("*ite = " << *ite); // 100

	it = v.begin(); // On replace l'it�rateur au d�but (pour pouvoir appeler advance)

	advance(it, 3); // advance => positionne l'it�rateur sp�cifi� � l'index sp�cifi�

	COUT("\nValeur point�e par l'it�rateur apr�s advance : " << *it); // 8 (4i�me �l�ment du vecteur)

	sort(v.begin(), v.end());

	COUT("Vecteur tri� : ");

	print_container<vector, int>(v); // 1 4 7 8 9 100

	COUT("v.clear(); ");

	v.clear();

	COUT("v.size() = " << v.size() << " - v.capacity() = " << v.capacity());

	COUT("v.empty() = " << boolalpha << v.empty()); // boolalpha => write true or false instead of 1 or 0

	// ----------------------------------------------------------
	// ------------------------- LIST ---------------------------
	// ----------------------------------------------------------

	TEXT_COLOR(GREEN);
	cout << "\n========= STL - list ==========\n" << endl;
	/*
	* ********* liste chain�e ******************
	*
	* Une liste cha�n�e ou liste li�e d�signe une collection ordonn�e et de taille arbitraire d'�l�ments de m�me type, dont la repr�sentation en m�moire est une succession de cellules faites :
	* - d'un contenu
	* - d'un pointeur vers une autre cellule.
	* L'ensemble des cellules ressemble donc � une cha�ne dont les maillons seraient les cellules.
	*
	* - Optimis�e pour insertion et suppression rapides
	* - Les �l�ments ne sont pas stock�e de mani�re contigue
	* - Ne supporte pas les acc�s al�atoires (contrairement au tableaux)
	* - Bidirectional it�rations ( one diretional for forward_list)
	*
	* https://youtu.be/je7zlYfJf7Q
	*/

	list<string> liste = { "bb", "cc" };

	list<string> liste2 = liste; // liste2 est une copie de liste

	liste.push_back("cc");
	liste.push_back("dd");
	print_container<list, string>(liste); // bb cc cc dd

	liste.push_front("aa");
	print_container<list, string>(liste); // aa bb cc cc dd

	liste.pop_back();
	print_container<list, string>(liste); // aa bb cc cc

	/* Pour ins�rer un �l�ment dans une liste... */

	// liste.insert(liste.begin() + 2, "inserted"); NOK : marche avec vector mais pas avec list...
	list<string>::const_iterator iit(liste.begin()); // Il faut d'abord cr�er l'it�rateur en partant du d�but

	advance(iit, 2); // Positionnement de l'it�rateur

	liste.insert(iit, "inserted"); // et enfin ins�rer... https://cplusplus.com/reference/list/list/insert/

	print_container<list, string>(liste); // aa bb inserted cc cc

	liste.remove("cc");

	print_container<list, string>(liste); // aa bb inserted

	//D�claration de 2 it�rateurs const. (1 au d�but, 1 � la fin)
	list<string>::const_iterator lit(liste.begin()), lend(liste.end());
	cout << endl;

	// Parcours d'une liste avec boucle for via iterateur
	for (; lit != lend; lit++) {
		cout << *lit << " ";
	}

	lit = liste.begin(); // On recale les it�rateurs pour parcourir une seconde fois avec boucle while
	lend = liste.end();

	// Parcours d'une liste avec boucle while via it�rateur
	cout << endl;
	while (lit != lend)
	{
		cout << *lit << " ";
		lit++;
	}

	// Parcours d'une liste avec boucle foreach sans se faire chier...
	cout << endl;
	for (auto l : liste)
	{
		cout << l << " ";
	}
	/*
	* - begin() returns an iterator that can be used to iterate through the collection,
	* - front() just returns a reference to the first element of the collection.
	*/
	cout << endl;
	COUT("liste.front() = " << liste.front()); // aa
	COUT("liste.back() = " << liste.back()); // inserted

	liste.remove("bb");
	liste.clear();

	COUT("liste.empty() = " << boolalpha << liste.empty());

	print_container<list, string>(liste2); // liste2 existe encore // bb cc

	// -----------------------------------------------------------
	// ------------------------- STACK ---------------------------
	// -----------------------------------------------------------

	TEXT_COLOR(YELLOW);
	COUT("\n========= STL - stack (LIFO) ==========\n");
	// https://youtu.be/WK97Pj0wa7A

	stack<int> s;

	for (size_t i = 0; i < 5; i++)
	{
		s.push(i);
	}

	COUT("stack size : " << s.size()); // 5
	COUT("stack top : " << s.top()); // 4
	COUT("stack empty : " << boolalpha << s.empty()); // false

	s.pop();
	COUT("stack top : " << s.top()); // 3

	// -----------------------------------------------------------
	// ------------------------- QUEUE ---------------------------
	// -----------------------------------------------------------
	TEXT_COLOR(WHITE);
	COUT("\n========= STL - queue (FIFO) ==========\n");
	// https://youtu.be/M73wcfBwX7Y

	queue<int> q;

	for (size_t i = 0; i < 5; i++)
	{
		q.push(i);
	}

	COUT("queue size : " << q.size()); // 5
	COUT("queue front : " << q.front() << " - queue back : " << q.back()); // 0 - 4
	COUT("queue empty : " << boolalpha << q.empty()); // false

	q.pop();
	q.pop();

	COUT("queue size : " << q.size()); // 3
	COUT("queue front : " << q.front() << " - queue back : " << q.back()); // 2 - 4

	//system("cls");

	// ---------------------------------------------------------
	// ------------------------- SET ---------------------------
	// ---------------------------------------------------------
	TEXT_COLOR(GREEN);
	COUT("\n========= STL - set ==========\n");
	// https://youtu.be/2OEnAdl1eLc
	//ensemble d'�l�ments tri�s et sans doublons

	set<int> mySet;
	mySet.insert(2);  // mySet = {2}
	mySet.insert(5);  // mySet = {2,5}
	mySet.insert(2);  // mySet = {2,5} le doublon n'est pas pris
	mySet.insert(1);  // mySet = {1,2,5}
	mySet.insert(10); // mySet = {1,2,5,10}

	print_container<set, int>(mySet); // 1 2 5 10

	auto ub = mySet.upper_bound(2); // 5

	/*
	* 'upper_bound' retourne un it�rateur pointant vers le premier �l�ment strictement sup�rieur � une certaine valeur
	* dans un certain intervalle d'�l�ments tri�s.
	*/
	COUT("upper_bound(2) = " << *ub); // 5

	ub = mySet.upper_bound(1); // 2

	COUT("upper_bound(1) = " << *ub); // 2

	auto sit = mySet.erase(mySet.find(2)); // retourne un it�rateur pointant sur l'emplacement supprim�

	print_container<set, int>(mySet); // 1 5 10

	COUT(*sit); // 5

	int numberOfErasedElements = mySet.erase(5); // 1

	COUT("numberOfErasedElements = " << numberOfErasedElements);
	print_container<set, int>(mySet); // 1 10

	vector<int> v2 = { 2 , 5 , 10 , 12, 14, 100 };

	mySet.insert(v2.begin(), v2.end());

	print_container<set, int>(mySet); // 1 2 5 10 12 14 100

	//system("cls");

	// ---------------------------------------------------------
	// ------------------------- MAP ---------------------------
	// ---------------------------------------------------------

	TEXT_COLOR(YELLOW);
	COUT("\n========= STL - map ==========\n");
	// map : tableau associatif de type cl�/valeur sans doublons dont les cl�s sont tri�es

	map<int, string> myMap = { {2, "fifi"} , {1, "riri"} };

	myMap.insert({ 3, "loulou" });

	myMap.insert({ 3, "not inserted because of already existing key" });

	map<int, string>::const_iterator mit;

	for (mit = myMap.begin(); mit != myMap.end(); mit++) {

		COUT("cl� : " << mit->first << " - valeur : " << mit->second);
	}

	cout << endl;

	map<int, string>::const_iterator itm = myMap.erase(myMap.find(2)); // rerourne l'it�rateur suivant l'�l�ment supprim� ( { 3, "loulou" })

	COUT("itm->first : " << itm->first << ", itm->second : " << itm->second); // 3 , loulou

	for (mit = myMap.begin(); mit != myMap.end(); mit++) {

		COUT("cl� : " << mit->first << " - valeur : " << mit->second);
	}

	cout << endl;

	for (auto& [key, value] : myMap) // c++ 17 => (project > Properties > C/C++ > Language > C++ Language Standard
	{
		COUT("cl� : " << key << " - valeur : " << value);
	}

	cout << endl;

	myMap.insert({ { 2, "fifi" }, { 4, "donald" } });

	for (auto& [key, value] : myMap)
	{
		COUT("cl� : " << key << " - valeur : " << value);
	}

	cout << endl;

	myMap.erase(4);

	for (auto& [key, value] : myMap)
	{
		COUT("cl� : " << key << " - valeur : " << value);
	}

	cout << endl;

	// Picsou n'est pas ins�r� car la cl� 3 existe d�j�
	map<int, string> myMap2 = { {5 , "daisy"},  {3, "picsou"}, {4, "donald"} };

	myMap.insert(myMap2.begin(), myMap2.end());

	for (auto& [key, value] : myMap)
	{
		COUT("cl� : " << key << " - valeur : " << value);
	}
}
