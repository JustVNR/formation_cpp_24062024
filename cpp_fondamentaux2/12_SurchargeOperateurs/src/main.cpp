#include "stdafx.h"
#include <string>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15
using namespace std;

/*
* Il existe 2 types de surcharges d'o�prateur :
* - interne : � privil�gier si la surcharge n�cessite d'acc�der � des attributs priv�s
* - externe : � privil�gier si la surcharge peut �tre mise en oeuvre via des attributs ou des m�thodes publiques
* 
* La surcharge interne est impossible pour les op�rateurs dont l'op�rande de gauche n'est pas du type consid�r�
* exemple : 3 * p => '3' n'est pas de type 'Point' => surcharge externe obligatoire
*/

class Point
{
    double _x;
    double _y;

    // friend : permet � l'impl�mentation de l'objet 'ami' d'acc�der aux attributs priv�s de la classe
    // Mauvaise pratique : viole le principe d'encapsulation
    friend const Point operator*(double mult, Point const& p);

public:
    double x() const
    {
        return _x;
    }

    void x(double value)
    {
        _x = value;
    }

    double y() const
    {
        return _y;
    }

    void y(double value)
    {
        _y = value;
    }

    // Constructeur

    //Point(double _x)
    //{
    //    // (*this)._x = _x;
    //    this->_x = _x;
    //}

    Point(double x, double y) :_x(x), _y(y){}

    // M�thodes

    Point add(Point const& other)
    {
        return Point(_x + other._x, _y + other._y);
    }

    Point multiply(double mult)
    {
        return Point(_x * mult, _y * mult);
    }

    string toString() const
    {
        return "(" + to_string(_x) + ", " + to_string(_y) + ")"; // to_string => #include <string>
    }

    // Surcharges INTERNES

    Point operator+(Point const& other) const
    {
        return Point(_x + other._x, _y + other._y);
    }

    Point operator*(double mult) const
    {
        return Point(_x * mult, _y * mult);
    }

    Point& operator+=(Point const& other)
    {
        _x += other._x;

        _y += other._y;

        // this : fait r�f�rence � l'instance courante
        return *this; // On retourne la valeur point�e par l'objet courant
    }
   
    bool operator==(Point const& other) const
    {
        return (/*this->*/_x == other._x && /*this->*/_y == other._y);
    }

    bool operator!=(Point const& other) const
    {
        return !(*this == other); // on d�finit '!=' � partir de '=='
    }
    
    /*
    * L'op�rateur d'affection (p1 = p2) est le seul op�rateur universel : il est fourni par d�faut pour toutes les classes.
    * Il effectue (comme le constructeur de copie par d�faut) une copie de surface. Si la classe contient des pointeurs ou autres allocations dynamiques, il faudra soit :
       - le red�finir pour faire une copie profonde
       - interdire son utilisation : Point& operator=(Point const& p) = delete;
    */
    // Point& operator=(Point const& p) = delete;

};

// Surcharges externes

// Pourquoi rajouter 'const' au d�but de la signature
// Pour emp�cher de modifier le r�sultat de l'op�ration
// - pour �viter d'avoie le droit d'�crire ++(3*p)
// - pour �viter d'avoie le droit d'�crire 3 * p = ...
const Point operator*(double mult, Point const& p)
{
    // pb : _x et _y sont priv�s
    // return Point(p._x * mult, p._y * mult);

    // M�thode 1 : on passe par les getters
    //return Point(p.x() * mult, p.y() * mult);

    // M�thode 2 : On acc�de directement aux attributs priv�s grace au mot cl� "friend"
    // CECI EST UNE TRES MAUVAISE PRATIQUE : CA VIOLE LE PRINCIPE D'ENCAPSULATION
    // return Point(p._x * mult, p._y * mult);

    // M�thode 3 : On appelle la surcharge avec les op�randes dans l'autre sens (possible grace � la surcharge interne de 'operator*'

    return p * mult;
}

// Pourquoi retourner ostream& ? POur pouvoir chainer les appels...
static ostream& operator<<(ostream& sortie, Point const& p) {
   return sortie << p.toString();
}

static std::ostream& operator<<(std::ostream& sortie, Point const& obj)
{
    return sortie << obj.toString();
}


int main()
{
    SetConsoleOutputCP(1252); 

    Point p1(2.0, 3.0);
    Point p2(12.0, -4.0);

    COUT(p1.toString());
    COUT(p2.toString());

    // Point p3 = p1.add(p2).multiply(3.0);
    Point p3 = (p1 + p2) * 3.0;

    p3 += p1;
 

    COUT(p3.toString());

    if (p1 == p1)
    {
        COUT("Les points sont identiques");
    }

    if (p1 != p2)
    {
        COUT("Les points sont diff�rents");
    }

    p1 = p2 * 3;
    p1 = 3 * p2; // 3.multiply(p2)

    std::cout << p1/*.toString()*/ <<  std::endl;
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

