#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* En programmation imp�rative/procedurale les traitements et les donn�es ne sont li�s par aucun lien s�mantique.
*
* La POO permet au contraire de regrouper les traitements et les donn�es au sein d'une seule et m�me entit�.
*
* Cela permet d'obtenir :
* - robustesse
* - modularit�
* - lisibilit�
* - maintebilit�
*
* La POO repose sur certains principes fondamentaux :
*
* - Encapsulation : regrouper (voire dissimuler) dans une seule et m�me entit�
*   - les donn�es (attributs)
*   - les traitements (m�thodes)
*
* - Abstraction : manipuler des donn�es de bas niveaus (largeur, longueur) aux travers d'entit�s abstraites de plus haut niveau (rectangle)
* - H�ritage
* - Polymorphisme
*
* Une classe permet de d�finir de nouveaux types de donn�es.
*
* Une classe est ou moule � partir duquel fabriquer des objets (instances de la classe) du type consid�r�.
*
*/

namespace figures
{
    class Rectangle
    {
        // private: implicite

        double _largeur; // attributs encapsul�s
        double _longueur = 0.0; // On peut initialiser l'attribut ici. Cette initialisation sera utilis�e si le constructeur appel�e l'initialise pas lui-m�me l'attribut.

    public:
        // Getters et Setters

        double largeur() const
        {
            return _largeur;
        }

        void largeur(double value)
        {
            _largeur = value;
        }

        double longueur() const
        {
            return _longueur;
        }

        void longueur(double value)
        {
            _longueur = value;
        }

        // Constructeurs

        /*Rectangle()
        {
            _largeur = 0.0;
            _longueur = 0.0;
        }*/

        /*Rectangle(double largeur, double longueur)
        {
            _largeur = largeur;
            _longueur = longueur;
        }*/

        // Constructeur avec liste d'initialisation
        Rectangle(double largeur/* = 2.0*/, double longueur/*= 4.0*/) : _largeur(largeur), _longueur(longueur) {}

        // Constructeur sans param�tre faisant appel au constructeur � 2 param�tres
        Rectangle() : Rectangle(2.0, 4.0) {}

        /*
        * Constructeur de copie
        * 
        * Un constructeur de copie par d�faut est automatiquement g�n�r� par le compilateur s'il n'est pas explicitement impl�ment�.
        * Le contructeur par d�faut op�re une copie membre � membre des attributs.
        * Il est cependant parfois n�cessaire de le red�finir, notamment lorsque certains de attributs sont des pointeurs. Auquel cas l'objet point� n'est pas copi�, seul son adresse est copi� par d�faut (copie de surface). Ce qui peut poser probl�me lors de la lib�ration des ressources. 
        * Ces derni�res pouvant �tre lib�r�s plusieurs fois, une fois pour l'original, une fois pour la copie.
        */

        Rectangle(Rectangle const& other) : _largeur(other._largeur), _longueur(other._longueur) {}
        
        // On peut interdire la recopie
        // Rectangle(Rectangle const& other) = delete;

        /*
        * Destructeur 
        * 
        * - Ne peut pas �tre appel� directement
        * - N'acc�pte pas de param�tre
        * - Pas de surcharge
        * - Appel� automatiquement par le garbage collector
        */

        ~Rectangle()
        {
            COUT("Rectangle destroyed");
        }

        double Surface() const
        {
            return _largeur * _longueur;
        }
    };

    class RectangleDyn
    {
        // Attributs d'instance
        double* _largeur = new double(2.0); 
        double* _longueur = new double(4.0);


        /*
        * Attribut de classe (static)
        * - est partag� par touesles instances de la classe
        * - DOIT ETRE INITIALISE EXPLICTEMENET EN DEHORS DE LA CLASSE
        */
        // static int _count = 0; Erreu : une variable staitque doit �tre initialis�e hors de la clase
        static int _count;

    public:
        RectangleDyn() {
            _count++; // Attention : ne sera pas incr�ment� par dafaut dans le constructeur de copie
        }

        ~RectangleDyn() {

            _count--;

            COUT("RectangleDyn Destroyed !");
            delete _largeur; // Va causer une exception si on utilise le constrcuteur de copie par d�faut
            delete _longueur; // Parce que le destructeur sera appel� plusieurs fois pour lib�rer les m�mes ressources
        }

        // Constructeur de copie

        RectangleDyn(RectangleDyn const& other)
        {
            _count++;

            _largeur = new double(*other._largeur); // deep copie : on ne se contente pas de copier la valeur du pointeur (et donc l'adresse qu'il pointe)
            _longueur = new double(*other._longueur); // On cr�e un nouveau pointeur et on l'initialise avec la valeur du pointeur original
        }

        double Surface() const
        {
            return *_largeur * *_longueur;
        }
        static int Count() // const : Erreur une m�thode statique ne peut pas �tre d�finie comme const
        {
            return _count;
        }
    };

    int RectangleDyn::_count = 0; // on ne peut pas initialiser un attribut statique � l'int�rieur de la classe
}

// la fonction pourrait s'appliquer � n'importe quoi d'aautre qu'un rectangle
static double surface(double largeur, double longueur)
{
    return largeur * longueur;
}

namespace figs = figures;

int main()
{
    TEXT_COLOR(WHITE);

    SetConsoleOutputCP(1252);

    // PROCEDURAL

    double largeur(2.0); // largeur et longueur ne sont li�s par aucun lien s�mantique
    double longueur(4.0);

    COUT("La surface du rectangle sans POO vaut : " << surface(largeur, longueur));

    // OBJET

    figs::Rectangle rect(2.0, 4.0);

    // rect._largeur = 1; // Erreur : largeur est d�sormais encapsul�

    rect.longueur(6.0); // setter

    COUT("rect.longeur() = " << rect.longueur()); // getter

    COUT("La surface de rect vaut : " << rect.Surface());

    // Recopie d'une instance : rect et rect2 sont 2 instances distinctes do,nt les attributs ont les m�mes valeurs
    figs::Rectangle rect2(rect);

    TEXT_COLOR(GREEN);
    COUT("\n************************************");
    COUT("******** ALLOCATION DYNAMIQUE *******");
    COUT("************************************\n");

    {
        figs::RectangleDyn rectDyn;

        COUT("rectDyn.Surface() = " << rectDyn.Surface());
    }

    COUT("\n\--------- COPIES --------------n");

    {

        COUT("Nombre d'instances de RectangleDyn : " << figs::RectangleDyn::Count()); // 0
        figs::RectangleDyn rectDyn;

        COUT("rectDyn.Surface() = " << rectDyn.Surface());

        COUT("Nombre d'instances de RectangleDyn : " << figs::RectangleDyn::Count()); // 1


        // rectDyn2 est une copie de rectDyn
        figs::RectangleDyn rectDyn2(rectDyn);

        COUT("rectDyn2.Surface() = " << rectDyn2.Surface());

        COUT("Nombre d'instances de RectangleDyn : " << figs::RectangleDyn::Count()); // 2
    }

    COUT("Nombre d'instances de RectangleDyn : " << figs::RectangleDyn::Count()); // 0


    TEXT_COLOR(WHITE);
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

