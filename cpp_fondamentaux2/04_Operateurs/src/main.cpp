#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

int main()
{
    SetConsoleOutputCP(1252); 

	TEXT_COLOR(WHITE);
	COUT("\n***************************************");
	COUT("************* ARITHMETIQUES ***********");
	COUT("***************************************\n");

	int a = 4, b = 6;

	COUT(a + b);
	COUT(a - b);
	COUT(a * b);
	COUT(a / b); // division enti�re
	COUT(a / (b* 1.0)); 
	COUT(a % b); // modulo : reste de la division enti�re

	int n;

	double x = 1.5;

	n = 3 * x;

	COUT(n); // 4 car n est d�clar� en tant qu'entier

	TEXT_COLOR(GREEN);
	COUT("\n***************************************");
	COUT("********* AFFECTATION COMPOSEE *********");
	COUT("***************************************\n");

	a += b;
	COUT(a);

	a -= b;
	COUT(a);

	a *= b;
	COUT(a);

	a /= b;
	COUT(a);

	TEXT_COLOR(GREEN);
	COUT("\n***************************************");
	COUT("************* INCREMENTATION ***********");
	COUT("***************************************\n");

	COUT(a); // 4
	COUT(a++); // 4 // post incr�mentation : afficga puis incr�mentation
	COUT(a); // 5
	COUT(++a); // 6 // pr� incr�mentation : incr�mentation puis affichage

	TEXT_COLOR(YELLOW);
	COUT("\n***************************************");
	COUT("************** COMPARAISON *************");
	COUT("***************************************\n");

	COUT((a == b));
	COUT((a > b));
	COUT((a >= b));
	COUT((a <= b));
	COUT((a < b));
	COUT((a != b));

	TEXT_COLOR(YELLOW);
	COUT("\n***************************************");
	COUT("***************** LOGIQUES *************");
	COUT("***************************************\n");

	COUT((a > b && a < b)); // ET
	COUT(( a > b || a < b)); // OU
	COUT((!(a < b))); // NON
	COUT((a > b ^ a < b)); // OU EXCLUSIF

	TEXT_COLOR(YELLOW);
	COUT("\n***************************************");
	COUT("***************** BINAIRES *************");
	COUT("***************************************\n");

	a = 5; // 0101
	b = 3; // 0011

	int result = a & b;

	COUT("a & b = " << result); // 1

	result = a | b;

	COUT("a | b = " << result); // 7

	result = a ^ b;

	COUT("a ^ b = " << result); // 6

	int resultLeft = a << 1; // resultLeft contient le r�sultat du d�callage � gauche de 'a'
	int resultRight = a >> 1; // resultRight contient le r�sultat du d�callage � droite de 'a'

	COUT("a << 1 = " << resultLeft);
	COUT("a >> 1 = " << resultRight);

}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

