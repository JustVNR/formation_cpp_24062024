#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15
using namespace std;

/*
* Un pointeur est une variable de type entier stockant une adresse m�moire.
*
* Un pointeur peut servir � :
* - r�f�rencer un m�me objet depuis diff�rentes parties du code sans avoir � le copier
* - manipuler un objet dont la dur�e de vie exc�de la port�e du bloc d'instructions courant
* - changer d'objet point�
*
* Il existe 3 types de pointeurs en c++ :
* - pointeurs "� la C" : les plus puissants et (donc) les plus dangereux
* - les r�f�rences : tr�s s�res mais fondamentalement diff�rentes des vrais pointeurs
* - les pointeurs intelligents (smart pointers) depuis c++11. D�sallouent la m�moire automatiquement
*   - unique_ptr : 2 unique_ptr diff�rents ne peuvent pas pointer la m�me adresse simultan�ment
*   - shared_ptr : plusieurs shared_ptr peuvent pointer la m�me adresse sans qu'aucun ne sache qu'elle n'est plus utilse aux autres
*   - weak_ptr : idem shared_ptr mais ne bloque pas la lib�ration de la m�moire
*/


void increment(int a)
{
	a++;
}

void incrementRef(int& a)
{
	a++;
}

/*
* Les diff�rences entre un pssage par pointeu et par r�f�rence sont que :
* - un poijnteur peut pointer sur null, pas une r�f�rence
* - un pointeur peut �tre r�assign�, pas une r�f�rence
*
* Si le param�tre est susceptible d'�tre null ou d'�tre r�assign�, il faudra faire un passage par pointeur.
*/
void incrementPointeur(int* a)
{
	(*a)++;
}

struct Personne
{
	Personne()
	{
		COUT("New Personne");
	}

	~Personne()
	{
		COUT("Delete Personne");
	}
};


class B;  // D�claration anticip�e de la classe B

class A {
public:
	A() { std::cout << "A Constructor\n"; }
	~A() { std::cout << "A Destructor\n"; }

	std::shared_ptr<B> b_ptr;  // shared_ptr vers B
};

class B {
public:
	B() { std::cout << "B Constructor\n"; }
	~B() { std::cout << "B Destructor\n"; }

	// std::shared_ptr<A> a_ptr;  // shared_ptr vers A
	std::weak_ptr<A> a_ptr;  // weak_ptr vers A
};

int main()
{
	SetConsoleOutputCP(1252);

	TEXT_COLOR(YELLOW);
	COUT("\n************************************");
	COUT("********** POINTEURS A LA C *********");
	COUT("************************************\n");

	int* ptr; // d�claration d'un pointeur sur type entier
	ptr = NULL; // Un pointeur peut ne pointer sur rien
	ptr = nullptr; // Depuis c++11

	int var = 8;

	ptr = &var; // Affectation de l'adresse de 'var' au pointeur 'ptr'

	*ptr = 10; // *ptr permet d'acc�der � la valeur point�e par 'ptr'. On parle de d�r�f�rencement

	COUT("var = " << var); // 10

	int dereferenced = *ptr;

	COUT("dereferenced = " << dereferenced); // 10

	int* ptr2 = nullptr;

	// *ptr2 = 10; // Exception thrown: write access violation. Impossible de d�r�f�rencer un pointeur qui ne pointe sur rien

	// un tableau � la C est un pointeur pointant le premier �l�ment du tableau
	int arr[] = { 1, 13, 25, 37, 45, 46 };

	int indice = 3;

	COUT("arr[" << indice << "] = " << *(arr + indice)); // 37

	char bufferStack[8]{}; // Allocation statique dans la "Pile" de 8 char non initialis�s
	char* buffer = new char[8]; // Allocation dynamique dans le tas. La variable "buffer" contient l'adresse du d�but du tableau dans le tas

	COUT(*buffer); // � : non initialis�

	*buffer = 'a';

	COUT(*buffer); // a

	*(buffer + 1) = 'b';
	COUT(*(buffer + 1)); // b
	COUT(*(buffer + 2)); // � : non initialis�

	memset(buffer, 'z', 8); // Remplissage de la varaible 'buffer' avec un block de 8 caract�res 'z'

	for (size_t i = 0; i < 8; i++)
	{
		cout << *(buffer + i) << " "; // z z z z z z z z 
	}

	char** ptrBuffer = &buffer; // 'ptrBuffer' est un pointeur de pointeur stockant l'adresse du pointeur 'buffer'
	COUT("");
	COUT("ptrBuffer = " << ptrBuffer);

	// lib�ration de la m�moire allou�e au pointeur 'buffer'
	delete[] buffer;
	// La zone m�moire a �t� lib�r�e MAIS le pointeur pointe encore dessus
	buffer = nullptr; // Il est donc pr�f�rable de le faire pointer sur null apr�s delete

	/*
	* Exo : Inverser les valeurs de 2 entiers en utilisant des pointeurs
	*/

	int n1 = 2;
	int n2 = 3;

	COUT("n1 = " << n1 << ", n2 = " << n2);

	//inversion
	int* p1 = &n1;
	int* p2 = &n2;

	int temp = *p1;

	*p1 = *p2;
	*p2 = temp;

	COUT("n1 = " << n1 << ", n2 = " << n2);

	TEXT_COLOR(GREEN);
	COUT("\n************************************");
	COUT("************* REFERENCES ************");
	COUT("************************************\n");

	/*
	* Une r�f�rence est un alias (du sucre syntaxique) masquant l'utilisation d'un pointeur poitant sur une variable EXISTANTE.
	* Une r�f�rence ne peut donc aps r�f�rencer null
	* Une r�f�rence ne peut pas changer de varaible r�f�renc�e
	*
	* ATTENTION :
	* Une r�f�rence N'EST PAS UNE VARIABLE. Mais une simple sur �tiquette sur variable.
	* Une r�f�rence ne peut donc elle-m�me pas �tre r�f�renc�e.
	*/

	int a;

	int& ref = a; // On peut d�sormais utiliser 'ref' comme s'il s'agissait de la variable qu'elle r�f�rence.

	ref = 15;

	COUT("ref = " << ref); // 15
	COUT("a = " << a); // 15

	/*
	* EXO : impl�menter une fonction qui incr�mente la variable a
	*/

	increment(a); // passage de param�tre par valeur => le param�tre est copi� => a n'est pas incr�ment�

	COUT("a = " << a); // 15

	incrementRef(a); // passage de param�tre par r�f�rence => a est incr�ment�

	COUT("a = " << a); // 16

	incrementPointeur(&a); // passage de param�tre par pointeur => a est incr�ment�

	COUT("a = " << a); // 17

	const int& refReadOnly = a; // r�f�rence constante

	// refReadOnly = 45; Erreur : impossible de modifier la valeur d'une variable via une r�f�rence constante

	a = 25; // toutes les r�f�rences susceptibles de r�f�rrencer "a" "changent de valeur"
	COUT("ref = " << ref); // 25
	COUT("refReadonly = " << refReadOnly); // 25

	int b = 10;

	ref = b; // ATTENTION !!! ref ne r�f�rence pas 'b' puisqu'elle r�f�rence d�j� 'a'. ref = b <=> a = b;

	COUT("a = " << a); // 10

	// Si on veut pouvoir changer de variable r�f�renc�e il faut utiliser un vrai pointeur

	int* refPtr = &a;

	*refPtr = 12; // a = 12

	refPtr = &b;

	*refPtr = 18; // b = 18

	TEXT_COLOR(YELLOW);
	COUT("\n************************************");
	COUT("*********** SMART POINTERS **********");
	COUT("************************************\n");

	/*
	* Les pointeurs intelligents introduits en c++ 11 effectuent eux-m�mes la d�sallocation de la m�moire au moment opportun via le garbage collector (ramasse miette)
	*
	*
	*/

	COUT("\n*********** UNIQUE PTR **********\n");

	/*
	* std::unique_ptr poss�de la ressource vers laquelle il mointe => �vite les probl�mes de propri�t� partag�e et simplifie la gestion des ressources
	*/

	{
		Personne* p = new Personne(); // Pointeur � la C

		delete p; // il faut lib�rer la m�moire manuellement

		p = nullptr;

		unique_ptr<Personne> uniqueP = make_unique<Personne>();

		//unique_ptr<Personne> uniqueP2 = uniqueP; // Erreur : 2 unique_ptr diff�rents ne peuvent pointer sur la m�me adresse
	}

	COUT("\n*********** SHARED PTR **********\n");

	{
		shared_ptr<Personne> sharedP;

		{
			shared_ptr<Personne> sharedP2 = make_shared<Personne>();

			sharedP = sharedP2; // autoris� puisqu'�tant de type shared_ptr
		}
		COUT("La m�moire allou�e pour sharedP2 n'a pas encore �t� d�salou�e car encore utile � sharedP");
	} // sharedP est lib�r� ici

	COUT("\n*********** WEAK PTR **********\n");
	// Permet d'�viter les risques de r�f�rences circulaires
	{
		weak_ptr<Personne> weakP;

		{
			weak_ptr<Personne> weakP2 = make_shared<Personne>();

			weakP = weakP2; // autoris� puisqu'�tant de type shared_ptr
		}
		COUT("La m�moire allou�e pour weakP2 est d�j� d�salou�e");
	}

	TEXT_COLOR(CYAN);
	COUT("\n******* REFERENCE CIRCULAIRE *******\n");

	{
		// Cr�ation d'instances de A et B
		std::shared_ptr<A> a = std::make_shared<A>();
		std::shared_ptr<B> b = std::make_shared<B>();

		// �tablissement de la r�f�rence circulaire
		a->b_ptr = b;
		b->a_ptr = a;
	}
	TEXT_COLOR(WHITE);


}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

