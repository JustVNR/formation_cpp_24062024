#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* Un tableau est un collectiojn de valeurs homog�nes ( de m�me type).
*
* Un tableau peut �tre :
* - statique : de taille fixe
* - dynamique : de taille variable => d�finis en c++ avec le type "vector" (depuis c++ 11)
*/

using namespace std;
#include <vector>
#include <array>

#include <algorithm>

static bool trouve(vector<int> vect, int n)
{
	for (auto& v : vect)
	{
		if (v == n) return true;
	}
	return false;
}

int main()
{
	SetConsoleOutputCP(1252);

	TEXT_COLOR(WHITE);
	COUT("\n***************************************");
	COUT("*************** DYNAMIQUES ************");
	COUT("***************************************\n");

	vector<int> myFirstVect; // Initialisation d'un tableau vide d'entiers

	// myFirstVect[0] = 5; // Erreur myFirstVect[0] n'existe pas puisqu'on n'a pas sp�cifi� la taille du tableau lors de son utilisation

	if (myFirstVect.empty())
	{
		myFirstVect.push_back(4); // ajoute un �l�ment en fin de tableau
		myFirstVect.push_back(5); // ajoute un �l�ment en fin de tableau

		COUT("myFirstVect.front() = " << myFirstVect.front()); //.front() retourne une r�f�rence vers le premier �l�ment du tableau

		COUT("myFirstVect.size() = " << myFirstVect.size()); // .size() retourne la taille du tableau

		COUT("myFirstVect.back() = " << myFirstVect.back()); //.back() retourne une r�f�rence vers le dernier �l�ment du tableau

		myFirstVect.pop_back(); // supprime le dernier �l�ment du tableau

		myFirstVect.clear(); // supprime tous les �l�ments du tableau

		COUT("myFirstVect.size() = " << myFirstVect.size()); // 0
	}

	// On peut d�finir la taille du tableau en l'initialisant

	vector<int> vect(5); // Initialisation d'un tableau dynamique de 5 entiers (par d�faut � 0)

	COUT("vect[4] = " << vect[4]); // 0

	vector<int> vectOnes(5, 1); // Initialisation d'un tableau dynamique de 5 entiers initialis�s � 1

	COUT("vectOnes[4] = " << vectOnes[4]); // 1

	vector<int> vectCopy(vectOnes); // Cr�ation d'un nouveau vecteur par copie d'un vecteur existant

	// vector<int> vectCopy = vectOnes; // syntaxe alternative

	vector<int> vectAges = { 20,35,40,75,80 };
	// vector<int> vectAges({ 20,35,40,75,80 }); // syntaxe alternative

	// sort(vectAges.begin(), vectAges.end());

	vectOnes = vectAges;

	vectAges[0] = 30;

	COUT("\nPARCOURS D'UN VECTEUR AVEC BOUCLE FOR :\n ");
	for (int i = 0; i < vectOnes.size(); i++)
	{
		COUT("vectOnes[" << i << "] = " << vectOnes[i] << ", vectAges[" << i << "] = " << vectAges[i]);
	}

	COUT("\nPARCOURS D'UN VECTEUR AVEC BOUCLE FOR EACH :\n ");

	for (auto item : vectOnes) // auto : inf�rence de type => le compilateur peut d�terminer le type de item � partir de celui de vectOnes
	{
		item++; // ATTENTION le tableau n'est pas modifi� car la variable 'item' est une copie de la cellule du tableau
		cout << item << " ";
	}
	COUT("\n--------------\n");
	for (auto item : vectOnes)
	{
		cout << item << " ";
	}
	COUT("\n--------------\n");
	for (auto& item : vectOnes)
	{
		item++; // le tableau est modifi� car passage par r�f�rence
		cout << item << " ";
	}

	COUT("\n--------------\n");
	for (auto item : vectOnes)
	{
		cout << item << " ";
	}

	myFirstVect.swap(vectAges); // Echange les 2 tableaux 

	COUT("\nTableaux dynamiques multi dimensionnels\n");

	vector<vector<int>> vect2D(5, vector<int>(6, 1)); // tableau dynamique de 5 tableaux dynamique de 6 entiers intialis�s � 1

	vect2D[2][3] = 0; // 3i�me ligne, 4i�me colonne

	for (auto& line : vect2D)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT(""); // retour � la ligne
	}

	// Un tableau de tableaux peut contenir des tableaux de tailles diff�rentes
	vect2D[1].push_back(5); // Ajoute 5 en fin de seconde ligne

	COUT("-------------------------");

	for (auto& line : vect2D)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}

	COUT("-------------------------");

	vector<vector<int>> vect2D2 = { {1, 2, 3}, {4,5,6,7 }, {8, 9} };

	for (auto& line : vect2D2)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}

	/*
	* Demander � l'utilisateur des nombres entiers tant qu'il souhaite en saisir.
	* Calculer et afficher la somme et la moyenne des nombres saisis.
	*/

	vector<int> nombres;
	int nombre;
	char continuer = 'N';

	/*do {
		cout << "Entrez un nombre entier : ";
		cin >> nombre;

		nombres.push_back(nombre);

		cout << "Voulez vous ajouter un autre nombre ? (O/N) : ";
		cin >> continuer;

	} while (toupper(continuer) == 'O');

	if (nombres.empty())
	{
		COUT("Aucun nombre saisi");
	}
	else
	{
		int somme = 0;
		for (int n : nombres)
		{
			somme += n;
		}

		double moyenne = (double)somme / nombres.size();

		COUT("Somme des nombres = " << somme);
		COUT("Moyenne des nombres = " << moyenne);
	}*/

	TEXT_COLOR(GREEN);
	COUT("\n***************************************");
	COUT("*************** STATIQUES *************");
	COUT("***************************************\n");

	/*
	* Un tableau statique a une taille fixe :
	*
	* Il existe 2 types de tableaux statiques :
	* - tableaux "� la 'C'"
	* - array (introduits en c++ 11)
	*
	* Les tableaux � la C :
	* - sont toujours pass�s par r�f�rence
	* - n'ont pas connaissance de leur propre taille
	* - ne peuvent pas �tre manipul�s globalement (pas de tab2 = tab1)
	* - ne peuvent pas �tre retourn�s par une fonction
	*/

	COUT("\n---------- TABLEAUX 'A LA C' ---------\n");

	const int size = 3;

	int cStyleArray[size]; // d�claration d'un tableau de "size" entiers non initialis�

	int cStyleArray2[size] = { 1, 2, 3 }; // d�claration d'un tableau de "size" entiers initialis�

	int count = sizeof(cStyleArray) / sizeof(cStyleArray[0]); // d�termination de la taille du tableau � post�riori

	for (int i = 0; i < count; i++)
	{
		cStyleArray[i] = i;
	}

	for (int i = 0; i < count; i++)
	{
		cout << cStyleArray[i] << " ";
	}

	COUT("\n--------- TABLEAUX 'A LA C' 2D -------\n");

	const int nrows = 3;
	const int ncols = 4;

	int cStyleArray2D[nrows][ncols];

	for (int i = 0; i < nrows; i++)
		for (int j = 0; j < ncols; j++)
			cStyleArray2D[i][j] = i + j;

	for (int i = 0; i < nrows; i++)
	{
		for (int j = 0; j < ncols; j++)
		{
			cout << cStyleArray2D[i][j] << " ";
		}
		COUT("");
	}

	TEXT_COLOR(YELLOW);
	COUT("\n----------- ARRAY (c++ 11) ---------\n");

	array<double, 4> array4; // d�claration d'un array (non initialis�) de 4 doubles

	for (auto& item : array4)
	{
		cout << item << " ";
	}


	array<unsigned int, 3> array3 = { 1, 2, 3 };
	// array<unsigned int, 3> array3({ 1, 2, 3 }); syntaxe alternative

	array<unsigned int, 3> array3b = array3; // recopie les valeurs de array3 dans array3b

	array3b[array3b.size() - 1] = 10; // Changement de valeur du dernier �l�ment de 'array3b'

	COUT("");
	for (int i = 0; i < array3b.size(); i++)
	{
		cout << array3b[i] << " "; // 1 2 10
	}

	COUT("\n---------- ARRAY DE ARRAYS --------\n");

	array<array<int, 2>, 4> array2D; // array de 4 arrays de 2 entiers
	array<array<array<int, 5>, 2>, 4> array3D; // array de 4 arrays de 2 arrays de 5 entiers

	array3D[0][0][0] = 1;

	// array2D = { {1, 2}, {3, 4}, {5, 6}, {7, 8} }; // ATTENTION : on ne peut pas imbriquer les accolades
	array2D = { 1, 2, 3, 4, 5, 6, 7, 8 };

	for (auto& line : array2D)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}

	/*
	* Utiliser un array pour stocker un ensemble de nombres entiers saisis par l'utilisateur.
	* Trouver et afficher l'�l�ment maximum dans le tableau.
	*/
	//const int taille = 5;  // Taille du tableau
	//array<int, taille> nombres2;

	//COUT("Entrez " << taille << " nombres entiers : ");

	//for (int i = 0; i < taille; i++)
	//{
	//	cout << "Nombre " << i + 1 << " : ";
	//	cin >> nombres2[i];
	//}

	//int maximum = nombres2[0];

	//for (int i = 1; i < taille; i++)
	//{
	//	if (nombres2[i] > maximum) maximum = nombres2[i];
	//}

	//COUT("Le maximum vaut " << maximum);

	/*
	* Utiliser un array pour stocker un ensemble de nombres entiers.
	* Trouvez et affichez tous les �l�ments uniques pr�sents dans le tableau.
	*/
	COUT("\n-------EXO DUPLICATES---------\n");
	array<int, 8> duplicates = { 1, 2, 2, 3, 4, 5 ,5 ,6 };

	vector<int> uniques;

	for (int n : duplicates)
	{
		if (!trouve(uniques, n))
		{
			uniques.push_back(n);
		}
	}

	cout << "Elements uniques dans le tableau : ";

	for (auto& item : uniques)
	{
		cout << item << " ";
	}

	TEXT_COLOR(GREEN);
	COUT("\n\n***************************************");
	COUT("**************** STRINGS **************");
	COUT("***************************************\n");

	string maChaine; // d�claration d'une chaine vide

	string message = "Hello World!";
	// string message("Hello World!"); // syntaxe alternative

	char myChar = 'c';

	// myChar = "c"; NOK

	maChaine = 'a';

	maChaine = "Hello";

	maChaine += " World"; // Concat�nation

	COUT(maChaine);

	maChaine[0] = 'h';

	COUT(maChaine);

	maChaine.append(" !!");

	COUT(maChaine);

	COUT("maChaine.size() = " << maChaine.size());

	maChaine.insert(5, " F___ING");

	COUT(maChaine);

	maChaine.replace(5, 8, " WONDERFULL");

	COUT(maChaine);

	COUT("");

	if (maChaine.find("o") != string::npos)
	{
		COUT("maChaine.find(\"o\") = " << maChaine.find("o")); // retourne l'indice de la premi�re occurence (string::npos si non trouv�)
		COUT("maChaine.rfind(\"o\") = " << maChaine.rfind("o"));// retourne l'indice de la derni�re occurence (string::npos si non trouv�)
	}

	COUT("maChaine.substr(6, 10) = " << maChaine.substr(6, 10)); // retourne une sous chaine de longueur 10 � partir de l'indice 6

	TEXT_COLOR(WHITE);
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

