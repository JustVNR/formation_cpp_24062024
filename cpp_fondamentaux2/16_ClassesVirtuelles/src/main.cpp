#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15
using namespace std;

/*
* https://www.youtube.com/watch?v=1Pcp5RRQwfM&list=PLeBlObWk0duQ3FDIm6tJNdNIsU15A43kG&index=4
*
* Il peut arriver qu'une classe soit incluse plusieurs fois dans une hi�rarchie de classes.
*
* - Exemple 1 : Ovovivipare h�rite de Ovipare et de Vivipare, ces derni�re h�ritant elles-m�mes d'une unique classe Animal.
* Ainsi Ovovivipare h�rite indirectement 2 fois de la classe animal.
*
* Les attributs et m�thodes de la classe Animal seront donc inclues 2 fois.
* => Chaque objet de la classe Ovovivipare poss�dera 2 copies des attributs de la classe Animal.
*
* - Exemple 2 : Une classe v�hicule est h�rit�e par 2 classes "Essence" et "Electrique",
* ces derni�res �tant elle-m�mes h�rit�es par une classe "Hybride" => Hybride h�rite 2 fois de V�hicule
*/

/*
* Pour �viter la duplication des attributs d'une super-classe plusieurs fois incluse lors d'h�ritages multiples,
* il faut d�clarer son lien d'h�ritage avec toutes ses sous-classes comme VIRTUEL.
* Cette super classe sera alors dite "virtuelle" ( � ne pas confondre avec une classe abstraite !!)
*
* Syntaxe :
* class NomSousClasse : public virtual NomSuperClasseVirtuelle
*
* Cette m�thode pose n�anmoins un pb de conception. Car bien que ce soit la classe Ovovivipare qui pose probl�me,
* Ce sont ses classes m�res (Ovipare et Vivipare) qui devront h�riter virtuellement de la classe Animal.
* Ainsi les concepteurs des classes Ovipare et Vivipare doivent-ils anticiper le fait qu'une classe puisse �ventuellement h�riter des 2.
* Ce probl�me de conception explique que nombre de langages (C#...) proscrivent l'h�ritage multiple.
*
* Rappel : dans un h�ritage usuel, le constructeur d'une sous-classe n'appelle que les constructeurs
* de ses super-classes imm�diates.
* Dans un h�ritage virtuel, la super classe virtuelle est initialis�e directement dans la sous-sous... classe instanci�e.
* => Toutes les sous-sous classes doivent donc explicitement appeler le constructeur de la super-super classe virtuelle.
*/

/*
* Dans une hi�rarchie de classes o� il existe des super-classes virtuelles:
* - le soin d'initialiser les super-classes virtuelles incombe � la sous-classe la plus d�riv�e.
* - les constructeurs des super-classes virtuelles sont invoqu�s en premier
* - les appels explicites aux constructeurs de la super-classe virtuelle dans les classes interm�diaires sont ignor�s.
* - ceux des classes non virtuelles le sont ensuite dans l'odre de d�claration de l'h�ritage.
* - l'ordre d'appel des constructeurs de copie est identique
* - l'ordre d'appel des destructeurs est l'inverse de celui des constructeurs.
*/

class Animal
{
protected:
	string _tete;

public:
	Animal(string const& description) : _tete(description) {}
};

class Ovipare : public virtual Animal
{
protected:
	unsigned int _nOeufs;

public:
	Ovipare(unsigned int nOeufs) : Animal("A cornes"), _nOeufs(nOeufs) {}
};

class Vivipare : public virtual Animal
{
protected:
	unsigned int _gestation;

public:
	Vivipare(unsigned int gestation) : Animal("de poisson"), _gestation(gestation) {}
};

class Ovovivipare : public Ovipare, public Vivipare
{
protected:
	bool _especeProtegee;

public:
	Ovovivipare(unsigned int nOeufs, unsigned int gestation, bool protege = false);

	virtual ~Ovovivipare() {};

	void afficher() const
	{
		//cout << "J'ai une tete " << Ovipare::tete << " et une tete " << Vivipare::tete << " !" << endl;
		COUT("J'ai une t�te " << _tete);
	}
};

Ovovivipare::Ovovivipare(unsigned int nOeufs, unsigned int gestation, bool protege)
	: Animal("� corne et de poisson"), Ovipare(nOeufs), Vivipare(gestation), _especeProtegee(protege)
{
	/*
	* Les appels au constructeur de "Animal" dnas les classes "Ovipare" et "Vivipare" sont ignor�s.
	* Ils sont remplac�s par l'appl direct au constructeur de "Animal".
	* Cet appel direct cout-circuite l'appel au constructeur d'Animal dans les clsses Vivipare et Ovovivipare.
	*/
}

int main()
{
    SetConsoleOutputCP(1252); 

	Ovovivipare o(3, 4, false);

	o.afficher();
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

