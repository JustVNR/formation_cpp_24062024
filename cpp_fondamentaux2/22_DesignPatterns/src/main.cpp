#include "stdafx.h"
#include "creation/singleton/Pdg.h"

#include "creation/prototype/Tree.h"
#include "creation/prototype/Forest.h"

#include "creation/builder/User.h"
#include "creation/builder/UserBuilder.h"

#include "creation/factory/Shape.h"
#include "creation/factory/Circle.h"
#include "creation/factory/Square.h"
#include "creation/factory/ShapeFactory.h"
#include "creation/factory/CircleFactory.h"
#include "creation/factory/SquareFactory.h"

#include "comportement/strategy/PaymentService.h"
#include "comportement/strategy/CreditCardStrategy.h"
#include "comportement/strategy/StripeStrategy.h"


#include "comportement/chainOfResponsability/Teacher.h"
#include "comportement/chainOfResponsability/HeadTeacher.h"
#include "comportement/chainOfResponsability/Director.h"
#include "comportement/chainOfResponsability/Complaint.h"

#include "comportement/memento/Editeur.h"

#include "comportement/iterator/CollectionLivres.h"

#include "structure/facade/GestionCommandeFacade.h"

#include "structure/adapter/PersonAdapter.h"

#include "structure/proxy/CompteBancaire.h"
#include "structure/proxy/ServiceBancaire.h"
#include "structure/proxy/ServiceBancaireProxy.h"

#include "structure/decorator/BaseKebab.h"
#include "structure/decorator/Salad.h"
#include "structure/decorator/Tomat.h"
#include "structure/decorator/Onion.h"

#include "structure/bridge/TV.h"
#include "structure/bridge/Radio.h"
#include "structure/bridge/Remote.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/***************************************************************
* AJOUTER $(ProjectDir) dans les propri�t�s du projets "C++ -> General -> Additional Include directories"
* Pour pouvoir faire des include depuis des sous dossiers
****************************************************************/
int main()
{
    SetConsoleOutputCP(1252); 

    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("********* SINGLETON (CREATION) ********");
    COUT("***************************************\n");

    // Le singleton est un pattern de cr�ation dont l'objectif
    // est de restreindre l'instanciation d'une classe
    // � un seul objet
    // Il est utilis� lorsqu'on a besoin exactement d'un objet
    // pour coordonner des op�rations dans un syst�me.

    Pdg* pdg1 = Pdg::getInstance("pdg1");
    Pdg* pdg2 = Pdg::getInstance("pdg2");

    COUT(pdg1->getName());
    COUT(pdg2->getName());

    TEXT_COLOR(GREEN);
    COUT("\n***************************************");
    COUT("******** PROTOTYPE (CREATION) *********");
    COUT("***************************************\n");

    try {
        Tree* treeProto = new Tree("green", 10.5f);
        Tree* treeClone = treeProto->clone();

        treeClone->setSize(5.5f);

        COUT(treeProto->toString() << " (proto)\n");
        COUT(treeClone->toString() << " (clone modifi�)\n");

        Forest* forestProto = new Forest();
        forestProto->addTree(treeProto);
        forestProto->addTree(treeClone);
        forestProto->addTree(treeProto);

        Forest* forestClone = forestProto->clone();
        forestClone->addTree(treeProto);

        COUT("For�t originale:\n" << forestProto->toString());
        COUT("For�t clon�e:\n" << forestClone->toString());

        delete treeProto;
        delete treeClone;
        delete forestProto;
        delete forestClone;
    }
    catch (std::bad_alloc& e) {
        std::cerr << "Erreur d'allocation m�moire : " << e.what() << std::endl;
    }

    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("***************** BUILDER *************");
    COUT("***************************************\n");

    /*
     * Le pattern Builder vise � faciliter la construction d'objets complexes en
     * �vitant d'avoir � leur associer un (trop) grand nombre de constructeurs.
     */

    User user = User::UserBuilder("nom", "prenom").age(25).address("adresse").build();
    User user2 = User::UserBuilder("nom", "prenom").age(50).phone("phone").build();

    COUT(user);
    COUT(user2);

    TEXT_COLOR(GREEN);
    COUT("\n***************************************");
    COUT("*********** FACTORY (CREATION) ********");
    COUT("***************************************\n");

    /*
     * Le pattern Factory permet de cr�er des objets sans exposer leur logique de
     * cr�ation � l'utilisateur et retourne les objets ainsi cr��s sous forme d'une
     * abstraction (classe abstraite ou interface). La classe exacte de l'objet n'est
     * donc pas connue par l'appelant.
     *
     * Exemple interface 'Shape'
     *
     * https://www.tutorialspoint.com/design_pattern/factory_pattern.htm
     *
     * https://www.youtube.com/watch?v=4az8MPlM5fQ
     */

    // Ordre :
    // - cr�er l'interface Shape.
    // - Cr�er ses impl�mentations Square et Circle
    // - Cr�er une classe ShapeFactory 
    // - Cr�er des impl�mentations de ShapeFatory : CircleFactory SquareFactory

    ShapeFactory* circleFactory = new CircleFactory();
    Shape* circle = circleFactory->getShape(10); // On ne connait pas le type pr�cis (Circle) retourn�...

    COUT("Circle surface: " << circle->getSurface());

    delete circle;
    delete circleFactory;

    ShapeFactory* squareFactory = new SquareFactory();
    Shape* square = squareFactory->getShape(10);

    COUT("Square surface: " << square->getSurface());

    delete square;
    delete squareFactory;

    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("******* STRATEGY (COMPORTEMENT) *******");
    COUT("***************************************\n");

    //	Le mod�le de conception Strategy est un mod�le comportemental permettant
    //	� un algorithme ou � une famille d'algorithmes de varier ind�pendamment 
    //	des clients qui les utilisent.
    //	
    //	Il d�finit une famille d'algorithmes, les encapsule, et les rend interchangeables.
    //	Les clients peuvent ainsi choisir l'algorithme appropri� sans changer leur code.

    //	Principes fondamentaux :
    //		
    //	- Encapsulation des Algorithmes :
    //		
    //		Les algorithmes sont encapsul�s dans des classes distinctes,
    //		appel�es strat�gies, ce qui permet de les rendre interchangeables.
    //	
    //	- Composition et D�composition :
    //		
    //		Le mod�le favorise la composition plut�t que l'h�ritage.
    //		Les clients peuvent �tre configur�s avec diff�rentes strat�gies
    //		pour composer des comportements vari�s.
    //	
    //	- Ind�pendance des Clients :
    //  
    //  Les clients ne d�pendent pas directement des d�tails
    //	des algorithmes. Ils d�pendent d'une interface commune, ce qui favorise
    //	la flexibilit� et l'extensibilit� du syst�me.

    // https://youtu.be/Nrwj3gZiuJU?si=iFgA7N-20z48zHuE
    // 
    //Cr�er l'interface IPaymentStrategy.
    //Cr�er l'interface IPaymentService.
    //Cr�er la classe PaymentService.
    //Cr�er les strat�gies de paiement StripeStrategy et CreditCardStrategy.

    IPaymentService* paymentService = new PaymentService(new CreditCardStrategy());
    paymentService->makePayment(50.0);

    // Paiement via Stripe
    paymentService->setPaymentStrategy(new StripeStrategy());
    paymentService->makePayment(30.0);

    delete paymentService;

    TEXT_COLOR(GREEN);
    COUT("\n********************************************");
    COUT("** CHAIN OF RESPONSIBILITY (COMPORTEMENT) **");
    COUT("********************************************\n");

    // Le pattern comportemental "Chain of Responsibility" (Cha�ne de responsabilit�) permet
    // de traiter une requ�te � travers une cha�ne de gestionnaires, chacun d�cidant s'il peut
    // traiter la requ�te ou s'il doit la transmettre au gestionnaire suivant dans la cha�ne.
    // 
    // Chaque gestionnaire a la possibilit� de traiter la requ�te ou de la passer au gestionnaire suivant.
    // 
    // Fonctionnement :
    //
    // Lorsqu'une requ�te est soumise, elle est pass�e le long de la cha�ne de gestionnaires.

    // Chaque gestionnaire d�cide s'il peut traiter la requ�te ou s'il doit la transmettre
    // au gestionnaire suivant.
    // La cha�ne forme une s�quence lin�aire o� chaque gestionnaire a la possibilit� de traiter
    // la requ�te ou de la passer au suivant.
    // Le client n'a pas � conna�tre les d�tails de la cha�ne et soumet simplement la requ�te
    // au premier gestionnaire.
    // 
    // Avantages :
    //
    // - D�compose la logique de traitement en petits gestionnaires ind�pendants.
    // - Permet d'ajouter ou de retirer des gestionnaires sans modifier le code client.
    // - Favorise la flexibilit� et la r�utilisation du code.
    // 
    // Inconv�nients :
    //
    // - La requ�te peut ne pas �tre trait�e si elle atteint la fin de la cha�ne 
    //   sans qu'aucun gestionnaire ne puisse la traiter.
    // - Les d�veloppeurs doivent s'assurer que chaque gestionnaire transmet correctement
    //   la requ�te au suivant.

    Teacher t("Prof", new HeadTeacher("Responsable p�dago", new Director("Directeur", nullptr)));

    t.handleComplaint(Complaint(145, ComplaintType::PROF, "req1", ComplaintState::OPENED));
    COUT("=================================");

    t.handleComplaint(Complaint(500, ComplaintType::PEDAGO, "req2", ComplaintState::OPENED));
    COUT("=================================");

    t.handleComplaint(Complaint(450, ComplaintType::DIRLO, "req3", ComplaintState::OPENED));
    COUT("=================================");


    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("******** MEMENTO (COMPORTEMENT) *******");
    COUT("***************************************\n");

    /*
     * Le pattern "Memento" est un mod�le de conception comportemental permettant de
     * capturer et de restaurer l'�tat interne d'un objet sans r�v�ler les d�tails
     * de son impl�mentation. Il est particuli�rement utile pour mettre en �uvre des
     * m�canismes de retour en arri�re (undo) ou de restauration d'�tat dans une
     * application.
     */

    Editeur editeur;
    editeur.setContent("a");
    editeur.setContent("ab");
    editeur.setContent("abc");

    COUT("Etat actuel de l'editeur: " << editeur.getContent()); // abc

    try {
        editeur.undo();
        COUT("1er restore : " << editeur.getContent()); // ab

        editeur.undo();
        COUT("2�me restore : " << editeur.getContent()); // a

        editeur.undo();
        COUT("3�me restore : " << editeur.getContent()); //

        editeur.undo();
        COUT("4i�me restore : " << editeur.getContent());
    }
    catch (const std::exception& e) {
        TEXT_COLOR(RED);
        std::cerr << e.what() << std::endl;
    }

    TEXT_COLOR(GREEN);
    COUT("\n***************************************");
    COUT("******* ITERATOR (COMPORTEMENT) *******");
    COUT("***************************************\n");

    CollectionLivres livres;

    livres.ajouterLivre("Livre A");
    livres.ajouterLivre("Livre B");
    livres.ajouterLivre("Livre C");

    IIterator* iterator = livres.creerIterator();

    while (iterator->hasNext()) {
        std::string titreLivre = iterator->suivant();
        std::cout << "Livre : " << titreLivre << std::endl;
    }

    delete iterator;

    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("********** FACADE (STRUCTURE) *********");
    COUT("***************************************\n");

    /* Le mod�le de conception "Facade" est un mod�le structurel qui fournit
     *
     * une interface unifi�e � un ensemble d'interfaces dans un sous-syst�me.
     *
     * Il d�finit une interface de niveau sup�rieur qui facilite l'utilisation
     *
     * du sous-syst�me en masquant la complexit� des composants individuels.
     *
     * Les fonctionnalit�s propos�es par la fa�ade seront plus limit�es que si vous
     *
     * interagissiez directement avec le sous-syst�me,
     *
     * mais vous pouvez vous contenter de n�inclure que les fonctionnalit�s qui int�ressent votre client.
     *
     * Une fa�ade se r�v�le tr�s pratique si votre application n�a besoin que d�une partie
     *
     * des fonctionnalit�s d�une librairie sophistiqu�e parmi les nombreuses qu�elle propose.
     *
     * Par exemple, une application qui envoie des petites vid�os de chats comiques
     *
     * sur des r�seaux sociaux peut potentiellement utiliser une librairie de conversion vid�o
     *
     * professionnelle.
     *
     * Mais il suffit d'une classe dot�e d�une m�thode encoder(fichier, format).
     *
     * Apr�s avoir cr�� cette classe et int�gr� la librairie de conversion vid�o,
     *
     * votre fa�ade est op�rationnelle.
     */

    GestionCommandeFacade gestionCommandeFacade;
    gestionCommandeFacade.traiterCommande("ProduitA", 5);

    TEXT_COLOR(GREEN);
    COUT("\n***************************************");
    COUT("********* ADAPTER (STRUCTURE) *********");
    COUT("***************************************\n");

    /*
      * L�Adaptateur est un patron de conception structurel qui permet � des objets
      * incompatibles de collaborer.
      *
      * L�adaptateur fait office d�emballeur (wrapper) entre les 2 objets.
      *
      * Il r�cup�re les appels � un objet et les met dans un formatcompatible
      *
      * avec le second.
      *
      */

    PersonApiService apiService;
    PersonAdapter personAdapter(apiService);
    Person p = personAdapter.getPerson();
    COUT(p);

    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("********** PROXY (STRUCTURE) **********");
    COUT("***************************************\n");

    /*
     * Un proxy est une classe se substituant � une autre classe.
     *
     * Par convention et simplicit�, le proxy impl�mente la m�me interface que la
     * classe � laquelle il se substitue.
     *
     * L'utilisation de ce proxy ajoute une indirection � l'utilisation de la classe
     *
     * � substituer.
     *
     * Un proxy est utilis� principalement pour contr�ler l'acc�s aux m�thodes de la
     * classe substitu�e.
     *
     * Il est �galement utilis� pour simplifier l'utilisation d'un objet � complexe
     * � � la base :
     *
     * par exemple, si l'objet doit �tre manipul� � distance (via un r�seau) ou si
     * l'objet est consommateur de temps.
     *
     * INDIRECTION : M�canisme permettant de connecter deux �l�ments entre eux
     *
     * en r�cup�rant l'adresse de l'un par la lecture de l'autre
     */

    CompteBancaire cb("aaa", 1000);
    ServiceBancaire sb;

    sb.retirerArgent(cb, 2000);
    sb.retirerArgent(cb, 500);

    ServiceBancaireProxy sbp(sb);
    sbp.retirerArgent(cb, 500);

    TEXT_COLOR(GREEN);
    COUT("\n***************************************");
    COUT("******** DECORATOR (STRUCTURE) ********");
    COUT("***************************************\n");

    /*
     * Le pattern Decorator permet de cr�er / modifier / d�corer un objet dynamiquement (en temps
     * r�el : ce que l'h�ritage ne permet pas). Plus flexible que l'h�ritage car
     * utilise la composition/agr�gation.
     *
     * https://youtu.be/j40kRwSm4VE?si=e8vUIngap9bJF9oX
     *
     * Cela permet par exemple de cr�er en temps r�el n'importe quel type de Kebab
     * sur base des ingr�dents disponibles. Sans avoir � pr�voir � l'avance toutes
     * les recettes possibles.
     *
     */

    IKebab* kebab = new Onion(new Tomat(new Salad(new BaseKebab())));

    COUT(kebab->toString());

    // Nettoyage de la m�moire
    delete kebab;

    TEXT_COLOR(YELLOW);
    COUT("\n***************************************");
    COUT("********* BRIDGE (STRUCTURE) **********");
    COUT("***************************************\n");

    /*
     * https://refactoring.guru/design-patterns/bridge
     * https://youtu.be/BjAfbfweuzk?si=xXh5dsvPpL66FdE1
     *
     * Le Pont est un patron de conception structurel qui scinde la logique m�tier
     * (ou une grande classe)
     *
     * en diff�rentes hi�rarchies de classes pouvant �voluer ind�pendamment.
     *
     * Une de ces hi�rarchies (souvent appel�e l�abstraction) gardera une r�f�rence
     *
     * vers un objet de la seconde hi�rarchie (l�impl�mentation) (relation
     * d'agr�gation).
     *
     * Puisque toutes les impl�mentations ont une interface commune, elles sont
     * interchangeables � l�int�rieur de l�abstraction. (l'abstraction Remote peut
     * changer d'impl�mentation device)
     *
     * Cas d'usage: Le Bridge pattern est particuli�rement utile dans le cas des
     * application cross plateformes qui supportent diff�rents type de SGBD ou qui
     * interragissent avec de multiples API d'un certain type (cloud platforms, social networks, etc.).
     *
     * Pont entre diff�rents appareils et t�l�commandes
     *
     * Dans cet exemple, nous allons voir la s�paration entre les classes des
     * t�l�commandes et celles des appareils qu�elles contr�lent.
     *
     * Les t�l�commandes prennent le r�le de l�abstraction et les appareils sont
     * leurs impl�mentations.
     *
     * Les m�mes t�l�commandes peuvent manipuler les diff�rents appareils.
     *
     * Le pont permet de modifier ou m�me de cr�er de nouvelles classes
     *
     * sans toucher au code de la hi�rarchie oppos�e.
     */

    IDevice* tv = new TV();
    IDevice* radio = new Radio();

    IRemote* remote = new Remote(tv);

    remote->changeChannel(5);

    remote->setDevice(radio);

    remote->changeChannel(5);

    delete tv;
    delete radio;
    delete remote;


    TEXT_COLOR(WHITE);
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

