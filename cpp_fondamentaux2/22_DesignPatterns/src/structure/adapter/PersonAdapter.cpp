#include "stdafx.h"
#include "PersonAdapter.h"
#include <sstream>

PersonAdapter::PersonAdapter(const PersonApiService& service) : service(service) {}

Person PersonAdapter::getPerson() {

    PersonApi apiPerson = service.getPerson();

    std::istringstream iss(apiPerson.getFullName());

    std::string firstName, lastName;

    iss >> firstName >> lastName;

    return Person(apiPerson.getId(), firstName, lastName);
}
