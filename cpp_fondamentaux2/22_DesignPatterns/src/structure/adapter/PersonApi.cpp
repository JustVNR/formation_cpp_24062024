#include "stdafx.h"
#include "PersonApi.h"

PersonApi::PersonApi(int id, const std::string& fullName) : Id(id), fullName(fullName) {}

int PersonApi::getId() const {
    return Id;
}

void PersonApi::setId(int id) {
    Id = id;
}

std::string PersonApi::getFullName() const {
    return fullName;
}

void PersonApi::setFullName(const std::string& fullName) {
    this->fullName = fullName;
}
