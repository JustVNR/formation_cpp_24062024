#pragma once
#include "Person.h"

class IPersonAdapter {
public:
    virtual ~IPersonAdapter() = default;
    virtual Person getPerson() = 0;
};
