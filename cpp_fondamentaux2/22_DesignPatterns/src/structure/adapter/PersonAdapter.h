#pragma once
#include "IPersonAdapter.h"
#include "PersonApiService.h"

class PersonAdapter : public IPersonAdapter {
private:
    PersonApiService service;

public:
    PersonAdapter(const PersonApiService& service);
    Person getPerson() override;
};
