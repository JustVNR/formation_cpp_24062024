#include "stdafx.h"
#include "Person.h"

Person::Person(int id, const std::string& firstName, const std::string& lastName)
    : Id(id), FirstName(firstName), LastName(lastName) {}

int Person::getId() const {
    return Id;
}

void Person::setId(int id) {
    Id = id;
}

std::string Person::getFirstName() const {
    return FirstName;
}

void Person::setFirstName(const std::string& firstName) {
    FirstName = firstName;
}

std::string Person::getLastName() const {
    return LastName;
}

void Person::setLastName(const std::string& lastName) {
    LastName = lastName;
}

// D�finition de l'op�rateur <<
std::ostream& operator<<(std::ostream& os, const Person& person) {
    os << "Person [Id=" << person.Id
        << ", FirstName=" << person.FirstName
        << ", LastName=" << person.LastName << "]";
    return os;
}
