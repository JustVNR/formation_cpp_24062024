#pragma once
#include <string>
#include <iostream>

class Person {
private:
    int Id;
    std::string FirstName;
    std::string LastName;

public:
    Person(int id, const std::string& firstName, const std::string& lastName);
    int getId() const;
    void setId(int id);
    std::string getFirstName() const;
    void setFirstName(const std::string& firstName);
    std::string getLastName() const;
    void setLastName(const std::string& lastName);

    // Surcharge de l'opérateur <<
    friend std::ostream& operator<<(std::ostream& os, const Person& person);
};
