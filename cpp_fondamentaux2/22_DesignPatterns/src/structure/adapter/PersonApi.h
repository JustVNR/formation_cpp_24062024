#pragma once
#include <string>

class PersonApi {
private:
    int Id;
    std::string fullName;

public:
    PersonApi(int id, const std::string& fullName);
    int getId() const;
    void setId(int id);
    std::string getFullName() const;
    void setFullName(const std::string& fullName);
};
