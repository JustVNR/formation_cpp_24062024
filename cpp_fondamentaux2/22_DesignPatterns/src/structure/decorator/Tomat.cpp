#include "stdafx.h"
#include "Tomat.h"
#include <iostream>

Tomat::Tomat(IKebab* newKebab) : IngredientDecorator(newKebab) {
    std::cout << "Ajout Tomate" << std::endl;
}

std::string Tomat::getDescription() const {
    return newKebab->getDescription() + ", Tomate";
}

double Tomat::getPrice() const {
    return newKebab->getPrice() + 2.3;
}
