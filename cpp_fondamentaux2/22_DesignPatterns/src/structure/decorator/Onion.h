#pragma once
#include "IngredientDecorator.h"

class Onion : public IngredientDecorator {
public:
    Onion(IKebab* newKebab);
    std::string getDescription() const override;
    double getPrice() const override;
};
