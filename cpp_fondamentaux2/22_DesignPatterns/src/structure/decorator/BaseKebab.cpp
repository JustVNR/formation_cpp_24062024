#include "stdafx.h"
#include "BaseKebab.h"
#include <iostream>

BaseKebab::BaseKebab() {
    std::cout << "Nouveau kebab" << std::endl;
}

std::string BaseKebab::getDescription() const {
    return "pain galette";
}

double BaseKebab::getPrice() const {
    return 3.00;
}

std::string BaseKebab::toString() const {
    return getDescription() + " coute " + std::to_string(getPrice()) + "�.";
}