#include "stdafx.h"
#include "IngredientDecorator.h"

IngredientDecorator::IngredientDecorator(IKebab* newKebab) : newKebab(newKebab) {}

std::string IngredientDecorator::getDescription() const {
    return newKebab->getDescription();
}

double IngredientDecorator::getPrice() const {
    return newKebab->getPrice();
}

std::string IngredientDecorator::toString() const {
    return getDescription() + " coute " + std::to_string(getPrice()) + "�.";
}
