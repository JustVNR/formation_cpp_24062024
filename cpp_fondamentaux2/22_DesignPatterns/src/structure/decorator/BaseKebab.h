#pragma once
#include "IKebab.h"

class BaseKebab : public IKebab {
public:
    BaseKebab();
    std::string getDescription() const override;
    double getPrice() const override;
    std::string toString() const override;
};
