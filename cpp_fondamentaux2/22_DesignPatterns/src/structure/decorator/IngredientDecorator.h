#pragma once
#include "IKebab.h"

class IngredientDecorator : public IKebab {
protected:
    IKebab* newKebab;

public:
    IngredientDecorator(IKebab* newKebab);
    std::string getDescription() const override;
    double getPrice() const override;
    std::string toString() const override;
};
