#include "stdafx.h"
#include "Onion.h"
#include <iostream>

Onion::Onion(IKebab* newKebab) : IngredientDecorator(newKebab) {
    std::cout << "Ajout Oignon" << std::endl;
}

std::string Onion::getDescription() const {
    return newKebab->getDescription() + ", Oignon";
}

double Onion::getPrice() const {
    return newKebab->getPrice() + 1.4;
}
