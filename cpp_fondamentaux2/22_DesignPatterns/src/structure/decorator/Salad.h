#pragma once

#include "IngredientDecorator.h"

class Salad : public IngredientDecorator {
public:
    Salad(IKebab* newKebab);
    std::string getDescription() const override;
    double getPrice() const override;
};
