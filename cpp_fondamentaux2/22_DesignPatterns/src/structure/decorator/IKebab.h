#pragma once
#include <string>

class IKebab {
public:
    virtual ~IKebab() = default;
    virtual std::string getDescription() const = 0;
    virtual double getPrice() const = 0;
    virtual std::string toString() const = 0;
};
