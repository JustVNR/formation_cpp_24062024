#pragma once
#include "IngredientDecorator.h"

class Tomat : public IngredientDecorator {
public:
    Tomat(IKebab* newKebab);
    std::string getDescription() const override;
    double getPrice() const override;
};
