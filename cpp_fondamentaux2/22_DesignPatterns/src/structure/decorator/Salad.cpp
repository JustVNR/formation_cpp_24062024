#include "stdafx.h"
#include "Salad.h"
#include <iostream>

Salad::Salad(IKebab* newKebab) : IngredientDecorator(newKebab) {
    std::cout << "Ajout Salade" << std::endl;
}

std::string Salad::getDescription() const {
    return newKebab->getDescription() + ", Salade";
}

double Salad::getPrice() const {
    return newKebab->getPrice() + 1.5;
}
