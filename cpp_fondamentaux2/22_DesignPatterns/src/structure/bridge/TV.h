#pragma once

#include "IDevice.h"
#include <iostream>

class TV : public IDevice {
public:
    void setVolume(int valeur) override;
    void setChannel(int channel) override;
    void turnOn() override;
    void turnOff() override;
};
