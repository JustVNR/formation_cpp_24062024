#pragma once
#include "IDevice.h"
#include <iostream>

class Radio : public IDevice {
public:
    void setVolume(int valeur) override;
    void setChannel(int channel) override;
    void turnOn() override;
    void turnOff() override;
};
