#include "stdafx.h"
#include "TV.h"

void TV::setVolume(int valeur) {
    std::cout << "tv volume updated to " << valeur << std::endl;
}

void TV::setChannel(int channel) {
    std::cout << "tv channel updated to " << channel << std::endl;
}

void TV::turnOn() {
    std::cout << "tv turned on" << std::endl;
}

void TV::turnOff() {
    std::cout << "tv turned off" << std::endl;
}
