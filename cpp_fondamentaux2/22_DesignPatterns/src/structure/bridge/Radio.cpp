#include "stdafx.h"
#include "Radio.h"

void Radio::setVolume(int valeur) {
    std::cout << "radio volume updated to " << valeur << std::endl;
}

void Radio::setChannel(int channel) {
    std::cout << "radio channel updated to " << channel << std::endl;
}

void Radio::turnOn() {
    std::cout << "radio turned on" << std::endl;
}

void Radio::turnOff() {
    std::cout << "radio turned off" << std::endl;
}
