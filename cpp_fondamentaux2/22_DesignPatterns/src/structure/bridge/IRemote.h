#pragma once

#include "IDevice.h"

class IRemote {
public:
    virtual ~IRemote() = default;
    virtual void changeVolume(int v) = 0;
    virtual void changeChannel(int c) = 0;
    virtual void setDevice(IDevice* device) = 0;
};
