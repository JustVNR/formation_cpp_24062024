#pragma once

#include "IRemote.h"

class Remote : public IRemote {
protected:
    IDevice* device;

public:
    Remote();
    Remote(IDevice* device);
    IDevice* getDevice() const;
    void setDevice(IDevice* device) override;
    void changeVolume(int v) override;
    void changeChannel(int c) override;
};
