#include "stdafx.h"
#include "Remote.h"

Remote::Remote() : device(nullptr) {}

Remote::Remote(IDevice* device) : device(device) {}

IDevice* Remote::getDevice() const {
    return device;
}

void Remote::setDevice(IDevice* device) {
    this->device = device;
}

void Remote::changeVolume(int v) {
    if (device) {
        device->setVolume(v);
    }
}

void Remote::changeChannel(int c) {
    if (device) {
        device->setChannel(c);
    }
}
