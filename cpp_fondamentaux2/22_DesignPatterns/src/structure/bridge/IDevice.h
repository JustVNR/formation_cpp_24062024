#pragma once

class IDevice {
public:
    virtual ~IDevice() = default;
    virtual void setVolume(int valeur) = 0;
    virtual void setChannel(int channel) = 0;
    virtual void turnOn() = 0;
    virtual void turnOff() = 0;
};
