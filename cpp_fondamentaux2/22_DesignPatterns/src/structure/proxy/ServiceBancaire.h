#pragma once
#include "IServiceBancaire.h"

class ServiceBancaire : public IServiceBancaire {
public:
    void retirerArgent(CompteBancaire& cb, int montant) override;
};
