#pragma once
#include "CompteBancaire.h"

class IServiceBancaire {
public:
    virtual void retirerArgent(CompteBancaire& cb, int montant) = 0;
    virtual ~IServiceBancaire() = default;
};
