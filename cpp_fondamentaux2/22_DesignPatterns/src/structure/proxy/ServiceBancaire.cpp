#include "stdafx.h"
#include "ServiceBancaire.h"
#include <iostream>

void ServiceBancaire::retirerArgent(CompteBancaire& cb, int montant) {
    if (cb.getSolde() >= montant) {
        cb.setSolde(cb.getSolde() - montant);
        std::cout << "Retrait de " << montant << " effectu�. Solde restant : " << cb.getSolde() << std::endl;
    }
    else {
        std::cout << "Fonds insuffisants. Retrait impossible." << std::endl;
    }
}
