#pragma once
#include <string>

class CompteBancaire {
private:
    std::string numero;
    double solde;

public:
    CompteBancaire(const std::string& numero, double solde);

    std::string getNumero() const;
    void setNumero(const std::string& numero);

    double getSolde() const;
    void setSolde(double solde);
};