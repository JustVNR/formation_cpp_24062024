#include "stdafx.h"
#include "ServiceBancaireProxy.h"
#include <iostream>

ServiceBancaireProxy::ServiceBancaireProxy(IServiceBancaire& serviceBancaire) : serviceBancaire(serviceBancaire) {}

void ServiceBancaireProxy::retirerArgent(CompteBancaire& cb, int montant) {
    if (cb.getSolde() < 100) {
        std::cout << "Le solde est inf�rieur � 100, l'op�ration est refus�e." << std::endl;
    }
    else {
        serviceBancaire.retirerArgent(cb, montant);
    }
}
