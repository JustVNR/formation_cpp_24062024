#include "stdafx.h"
#include "CompteBancaire.h"

CompteBancaire::CompteBancaire(const std::string& numero, double solde) : numero(numero), solde(solde) {}

std::string CompteBancaire::getNumero() const {
    return numero;
}

void CompteBancaire::setNumero(const std::string& numero) {
    this->numero = numero;
}

double CompteBancaire::getSolde() const {
    return solde;
}

void CompteBancaire::setSolde(double solde) {
    this->solde = solde;
}
