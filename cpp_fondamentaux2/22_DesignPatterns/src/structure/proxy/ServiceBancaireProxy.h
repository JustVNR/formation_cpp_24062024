#pragma once
#include "IServiceBancaire.h"

class ServiceBancaireProxy : public IServiceBancaire {
private:
    IServiceBancaire& serviceBancaire;

public:
    ServiceBancaireProxy(IServiceBancaire& serviceBancaire);
    void retirerArgent(CompteBancaire& cb, int montant) override;
};
