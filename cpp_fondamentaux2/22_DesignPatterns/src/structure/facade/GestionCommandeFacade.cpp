#include "stdafx.h"
#include "GestionCommandeFacade.h"
#include <iostream>

GestionCommandeFacade::GestionCommandeFacade() {}

void GestionCommandeFacade::traiterCommande(const std::string& produit, int quantite) {
    stockService.verifierStock(produit);
    commandeService.passerCommande(produit, quantite);
    facturationService.genererFacture(produit, quantite);
    std::cout << "Commande trait�e avec succ�s." << std::endl;
}
