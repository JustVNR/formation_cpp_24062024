#pragma once
#include <string>
#include "StockService.h"
#include "CommandeService.h"
#include "FacturationService.h"

class GestionCommandeFacade {
private:
    StockService stockService;
    CommandeService commandeService;
    FacturationService facturationService;

public:
    GestionCommandeFacade();
    void traiterCommande(const std::string& produit, int quantite);
};
