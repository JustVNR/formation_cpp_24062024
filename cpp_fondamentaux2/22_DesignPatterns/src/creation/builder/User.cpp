#include "stdafx.h"
#include "User.h"

User::User() : age(0) {}

std::string User::getFirstName() const {
    return firstName;
}

void User::setFirstName(const std::string& firstName) {
    this->firstName = firstName;
}

std::string User::getLastName() const {
    return lastName;
}

void User::setLastName(const std::string& lastName) {
    this->lastName = lastName;
}

int User::getAge() const {
    return age;
}

void User::setAge(int age) {
    this->age = age;
}

std::string User::getPhone() const {
    return phone;
}

void User::setPhone(const std::string& phone) {
    this->phone = phone;
}

std::string User::getAddress() const {
    return address;
}

void User::setAddress(const std::string& address) {
    this->address = address;
}

void User::display(std::ostream& os) const {
    os << "User [firstName=" << firstName << ", lastName=" << lastName
        << ", age=" << age << ", phone=" << phone << ", address=" << address << "]";
}

std::ostream& operator<<(std::ostream& os, const User& user) {
    user.display(os);
    return os;
}
