#include "stdafx.h"
#include "UserBuilder.h"

User::UserBuilder::UserBuilder(const std::string& firstName, const std::string& lastName) {
    user.firstName = firstName;
    user.lastName = lastName;
}

User::UserBuilder& User::UserBuilder::age(int age) {
    user.age = age;
    return *this; // Permet de cha�ner les appels de m�thodes
}

User::UserBuilder& User::UserBuilder::phone(const std::string& phone) {
    user.phone = phone;
    return *this;
}

User::UserBuilder& User::UserBuilder::address(const std::string& address) {
    user.address = address;
    return *this;
}

User User::UserBuilder::build() {
    return user;
}
