#pragma once

#include <string>

class User {
private:
    std::string firstName;
    std::string lastName;
    int age;
    std::string phone;
    std::string address;

public:
    User();

    std::string getFirstName() const;
    void setFirstName(const std::string& firstName);

    std::string getLastName() const;
    void setLastName(const std::string& lastName);

    int getAge() const;
    void setAge(int age);

    std::string getPhone() const;
    void setPhone(const std::string& phone);

    std::string getAddress() const;
    void setAddress(const std::string& address);

    class UserBuilder; // Forward declaration

    friend std::ostream& operator<<(std::ostream& os, const User& user);

private:
    void display(std::ostream& os) const;
};

