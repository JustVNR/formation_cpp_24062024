#pragma once

#include "User.h"

class User::UserBuilder {
private:
    User user;

public:
    UserBuilder(const std::string& firstName, const std::string& lastName);

    UserBuilder& age(int age);
    UserBuilder& phone(const std::string& phone);
    UserBuilder& address(const std::string& address);

    User build();
};

