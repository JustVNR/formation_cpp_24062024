#include "stdafx.h"
#include "Forest.h"

Forest::Forest() {}

Forest::~Forest() {

    for (Tree* tree : trees) {

        delete tree;
    }

    trees.clear();
}

void Forest::addTree(Tree* tree) {

    trees.push_back(tree->clone());
}

std::vector<Tree*> Forest::getTrees() const {
    return trees;
}

Forest* Forest::clone() const {

    Forest* f = new Forest();

    for (Tree* tree : trees) {
        f->addTree(tree);
    }
    return f;
}

std::string Forest::toString() const {

    std::string result = "";

    for (Tree* tree : trees) {

        result += tree->toString() + "\n";
    }

    return result;
}
