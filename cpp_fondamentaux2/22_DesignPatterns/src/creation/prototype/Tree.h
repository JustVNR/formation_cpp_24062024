#pragma once

#include <string>

class Tree {
private:
    std::string color;
    float size;

public:
    Tree();
    Tree(const std::string& color, float size);
    Tree(const Tree& other); // Constructeur de copie

    std::string getColor() const;
    void setColor(const std::string& color);
    float getSize() const;
    void setSize(float size);

    Tree* clone() const; // M�thode de clonage

    std::string toString() const;
};