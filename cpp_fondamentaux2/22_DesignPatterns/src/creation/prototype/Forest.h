#pragma once

#include <vector>
#include "Tree.h"

class Forest {
private:
    std::vector<Tree*> trees;

public:
    Forest();
    ~Forest();

    void addTree(Tree* tree);
    std::vector<Tree*> getTrees() const;

    Forest* clone() const;

    std::string toString() const;
};

