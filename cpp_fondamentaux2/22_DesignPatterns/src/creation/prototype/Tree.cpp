#include "stdafx.h"

#include "Tree.h"

Tree::Tree() : size(0.0f) {}

Tree::Tree(const std::string& color, float size)
    : color(color), size(size) {}

Tree::Tree(const Tree& other)
    : color(other.color), size(other.size) {}

std::string Tree::getColor() const {
    return color;
}

void Tree::setColor(const std::string& color) {
    this->color = color;
}

float Tree::getSize() const {
    return size;
}

void Tree::setSize(float size) {
    this->size = size;
}

Tree* Tree::clone() const {
    return new Tree(*this);
}

std::string Tree::toString() const {
    return "Tree [color=" + color + ", size=" + std::to_string(size) + "]";
}
