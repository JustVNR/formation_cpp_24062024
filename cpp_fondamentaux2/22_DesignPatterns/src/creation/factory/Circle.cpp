#include "stdafx.h"
#include "Circle.h"
#include <cmath>

//#ifndef M_PI
//#define M_PI 3.14159265358979323846
//#endif
double M_PI = std::acos(-1.0);

Circle::Circle(double radius) : radius(radius) {}

double Circle::getSurface() const {
    return M_PI * radius * radius;
}