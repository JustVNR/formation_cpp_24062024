#pragma once
#include "Shape.h"
class Square : public Shape
{
private:
    double size;

public:
    Square(double size);

    double getSurface() const override;
};

