#pragma once

class Shape {
public:
    virtual double getSurface() const = 0; // M�thode virtuelle pure
    virtual ~Shape() {} // Destructeur virtuel par d�faut
};