#pragma once
#include "Shape.h"

class ShapeFactory {
public:
    virtual Shape* getShape(double size) = 0; // M�thode virtuelle pure
    virtual ~ShapeFactory() {} // Destructeur virtuel par d�faut
};