#include "stdafx.h"
#include "SquareFactory.h"

Shape* SquareFactory::getShape(double size) {
    return new Square(size);
}