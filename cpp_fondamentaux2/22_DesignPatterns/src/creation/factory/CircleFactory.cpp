#include "stdafx.h"
#include "CircleFactory.h"

Shape* CircleFactory::getShape(double size) {
    return new Circle(size);
}