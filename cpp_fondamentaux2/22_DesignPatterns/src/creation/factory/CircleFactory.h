#pragma once
#include "ShapeFactory.h"
#include "Circle.h"

class CircleFactory : public ShapeFactory {
public:
    Shape* getShape(double size) override;
};