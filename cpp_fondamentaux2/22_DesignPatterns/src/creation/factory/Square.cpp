#include "stdafx.h"
#include "Square.h"

Square::Square(double size) : size(size) {}

double Square::getSurface() const {
    return size * size;
}