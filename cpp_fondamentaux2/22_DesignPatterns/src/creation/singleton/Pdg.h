#pragma once

#include <string>
#include <mutex>
#include <memory>

// Le singleton est un pattern de cr�ation dont l'objectif
// est de restreindre l'instanciation d'une classe
// � un seul objet (ou bien � quelques objets seulement).
// Il est utilis� lorsqu'on a besoin exactement d'un objet
// pour coordonner des op�rations dans un syst�me.

class Pdg {
private:
    std::string name;

    // L'utilisation d'un pointeur unique garantit que l'instance est d�truite lorsque le programme se termine.
    static std::unique_ptr<Pdg> instance;
    static std::mutex instanceMutex;

    // La pr�sence d'un constructeur priv� supprime le constructeur public par d�faut.
    // Seul le singleton peut s'instancier lui-m�me.
    Pdg();

public:
    // M�thode permettant de renvoyer une instance de la classe Singleton
    // Utilise std::call_once pour garantir l'initialisation unique du singleton.
    static Pdg* getInstance(const std::string& name);

    std::string getName() const;
};
