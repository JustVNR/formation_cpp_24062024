#include "stdafx.h"
#include "Pdg.h"

// Initialisation des membres statiques
std::unique_ptr<Pdg> Pdg::instance;
std::mutex Pdg::instanceMutex;

// Constructeur priv�
Pdg::Pdg() {}

// M�thode permettant de renvoyer une instance de la classe Singleton
Pdg* Pdg::getInstance(const std::string& name) {

    if (instance == nullptr)
    {
        // instance = std::make_unique<Pdg>(); Erreur : make_unique a besoin d'un constructeur public

        instance.reset(new Pdg());
    }

    // Utilisation d'un verrou pour prot�ger l'acc�s concurrent
    //std::lock_guard est un objet RAII(Resource Acquisition Is Initialization)
    //qui verrouille le mutex lors de sa cr�ation et le d�verrouille automatiquement
    //lorsque l'objet std::lock_guard sort de port�e (� la fin du bloc dans lequel il est d�clar�).
    //Cela assure que le mutex est toujours correctement d�verrouill�, m�me si une exception est lev�e.
    std::lock_guard<std::mutex> lock(instanceMutex);

    instance->name = name;

    return instance.get();
}

// M�thode pour obtenir le nom
std::string Pdg::getName() const {
    return name;
}
