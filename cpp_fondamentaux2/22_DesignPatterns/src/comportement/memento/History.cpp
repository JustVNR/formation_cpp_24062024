#include "stdafx.h"
#include "History.h"
#include <stdexcept>

History::History() {
    states.push_back(std::make_shared<EditeurState>(""));
}

void History::push(std::shared_ptr<EditeurState> state) {
    states.push_back(state);
}

void History::pop() {
    if (states.size() <= 1) {
        throw std::runtime_error("L'historique est vide");
    }
    states.pop_back();
}

std::string History::getCurrentContent() const {
    return states.back()->getContent();
}
