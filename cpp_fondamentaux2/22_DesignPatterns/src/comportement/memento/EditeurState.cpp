#include "stdafx.h"
#include "EditeurState.h"

EditeurState::EditeurState(const std::string& content) : content(content) {}

std::string EditeurState::getContent() const {
    return content;
}

void EditeurState::setContent(const std::string& content) {
    this->content = content;
}
