#include "stdafx.h"
#include "Editeur.h"

Editeur::Editeur() {
   // history.push(std::make_shared<EditeurState>(""));
}

std::string Editeur::getContent() const {
    return history.getCurrentContent();
}

void Editeur::setContent(const std::string& content) {
    history.push(std::make_shared<EditeurState>(content));
}

void Editeur::undo() {
    history.pop();
}
