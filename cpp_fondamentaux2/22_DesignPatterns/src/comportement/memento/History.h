#pragma once
#include <vector>
#include <memory>
#include "EditeurState.h"

class History {
private:
    std::vector<std::shared_ptr<EditeurState>> states;

public:
    History();
    void push(std::shared_ptr<EditeurState> state);
    void pop();
    std::string getCurrentContent() const;
};
