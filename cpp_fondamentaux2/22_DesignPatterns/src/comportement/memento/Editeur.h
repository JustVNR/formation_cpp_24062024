#pragma once
#include <memory>
#include "History.h"

class Editeur {
private:
    History history;

public:
    Editeur();
    std::string getContent() const;
    void setContent(const std::string& content);
    void undo();
};
