#pragma once
#include <string>

class EditeurState {
private:
    std::string content;

public:
    EditeurState(const std::string& content);
    std::string getContent() const;
    void setContent(const std::string& content);
};
