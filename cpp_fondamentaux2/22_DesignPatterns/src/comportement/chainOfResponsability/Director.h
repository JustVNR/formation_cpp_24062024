#pragma once
#include "Staff.h"

class Director : public Staff {
public:
    Director(const std::string& name, Staff* successor);
    void handleComplaint(const Complaint& req) override;
};
