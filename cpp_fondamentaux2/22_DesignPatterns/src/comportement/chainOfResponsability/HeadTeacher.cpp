#include "stdafx.h"
#include "HeadTeacher.h"
#include <iostream>

HeadTeacher::HeadTeacher(const std::string& name, Staff* successor) : Staff(name, successor) {}

void HeadTeacher::handleComplaint(const Complaint& req) {
    if (req.getType() == ComplaintType::PEDAGO) {
        std::cout << "Req trait�e par le responsable p�dagogique...." << std::endl;
        req.setState(ComplaintState::CLOSED);
    }
    else if (getSuccessor() != nullptr) {
        std::cout << "Req transmise au directeur...." << std::endl;
        getSuccessor()->handleComplaint(req);
    }
}
