#pragma once
#include "Staff.h"

class HeadTeacher : public Staff {
public:
    HeadTeacher(const std::string& name, Staff* successor);
    void handleComplaint(const Complaint& req) override;
};
