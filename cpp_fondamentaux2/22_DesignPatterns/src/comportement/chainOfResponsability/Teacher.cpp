#include "stdafx.h"
#include "Teacher.h"
#include <iostream>

Teacher::Teacher(const std::string& name, Staff* successor) : Staff(name, successor) {}

void Teacher::handleComplaint(const Complaint& req) {
    if (req.getType() == ComplaintType::PROF) {
        std::cout << "Req trait�e par le prof...." << std::endl;
        req.setState(ComplaintState::CLOSED);
    }
    else if (getSuccessor() != nullptr) {
        std::cout << "Req transmise au responsable p�dago...." << std::endl;
        getSuccessor()->handleComplaint(req);
    }
}
