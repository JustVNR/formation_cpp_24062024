#pragma once
#include <string>

/*
* enum vs enum class
* 
* enum
*   - Scope : Les valeurs d'un enum sont import�es dans l'espace de noms englobant, ce qui signifie
* qu'elles sont accessibles sans pr�fixe.
*   - Type safety : Les enum classiques ne sont pas fortement typ�s.Cela peut entra�ner des probl�mes lorsque des valeurs d'�num�rations diff�rentes peuvent �tre compar�es ou assign�es les unes aux autres.
*   - Implicit conversions : Les valeurs d'un enum peuvent �tre implicitement converties en int et vice versa.
* 
* enum class
* 
* - Scope : Les valeurs d'un enum class sont contenues dans l'espace de noms de l'�num�ration. Elles doivent �tre acc�d�es en utilisant le nom de l'�num�ration comme pr�fixe.
* - Type safety : Les enum class sont fortement typ�s, emp�chant les comparaisons et les assignations entre des �num�rations diff�rentes.
* - Implicit conversions : Les valeurs d'un enum class ne peuvent pas �tre implicitement converties en int. Une conversion explicite est n�cessaire.
*/

enum class ComplaintType { PROF, PEDAGO, DIRLO };
enum class ComplaintState { OPENED, CLOSED };

class Complaint {
private:
    int studentId;
    ComplaintType type;
    std::string message;
    mutable ComplaintState state; // mutable to allow modification in const methods

public:
    Complaint(int studentId, ComplaintType type, const std::string& message, ComplaintState state);

    int getStudentId() const;
    void setStudentId(int studentId);

    ComplaintType getType() const;
    void setType(ComplaintType type);

    const std::string& getMessage() const;
    void setMessage(const std::string& message);

    ComplaintState getState() const;
    void setState(ComplaintState state) const;
};
