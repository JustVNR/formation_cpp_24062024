#pragma once
#include "Staff.h"

class Teacher : public Staff {
public:
    Teacher(const std::string& name, Staff* successor);
    void handleComplaint(const Complaint& req) override;
};
