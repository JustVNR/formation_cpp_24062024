#pragma once
#include <string>
#include "Complaint.h"

class Staff {
private:
    std::string name;
    Staff* successor;

public:
    Staff(const std::string& name, Staff* successor);
    virtual ~Staff() = default;

    const std::string& getName() const;
    void setName(const std::string& name);

    Staff* getSuccessor() const;
    void setSuccessor(Staff* successor);

    virtual void handleComplaint(const Complaint& req) = 0;
};
