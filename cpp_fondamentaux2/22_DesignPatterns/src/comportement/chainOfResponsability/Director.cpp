#include "stdafx.h"
#include "Director.h"
#include <iostream>

Director::Director(const std::string& name, Staff* successor) : Staff(name, successor) {}

void Director::handleComplaint(const Complaint& req) {
    std::cout << "requ�te trait�e par le directeur...." << std::endl;
    req.setState(ComplaintState::CLOSED);
}
