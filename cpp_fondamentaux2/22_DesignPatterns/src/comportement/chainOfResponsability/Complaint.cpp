#include "stdafx.h"
#include "Complaint.h"

Complaint::Complaint(int studentId, ComplaintType type, const std::string& message, ComplaintState state)
    : studentId(studentId), type(type), message(message), state(state) {}

int Complaint::getStudentId() const {
    return studentId;
}

void Complaint::setStudentId(int studentId) {
    this->studentId = studentId;
}

ComplaintType Complaint::getType() const {
    return type;
}

void Complaint::setType(ComplaintType type) {
    this->type = type;
}

const std::string& Complaint::getMessage() const {
    return message;
}

void Complaint::setMessage(const std::string& message) {
    this->message = message;
}

ComplaintState Complaint::getState() const {
    return state;
}

void Complaint::setState(ComplaintState state) const {
    this->state = state;
}
