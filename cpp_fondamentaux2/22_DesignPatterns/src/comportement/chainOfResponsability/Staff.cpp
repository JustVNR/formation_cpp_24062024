#include "stdafx.h"
#include "Staff.h"

Staff::Staff(const std::string& name, Staff* successor) : name(name), successor(successor) {}

const std::string& Staff::getName() const {
    return name;
}

void Staff::setName(const std::string& name) {
    this->name = name;
}

Staff* Staff::getSuccessor() const {
    return successor;
}

void Staff::setSuccessor(Staff* successor) {
    this->successor = successor;
}
