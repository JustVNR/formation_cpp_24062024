#pragma once

#include <string>

class IIterator {
public:
    virtual ~IIterator() = default;
    virtual bool hasNext() = 0;
    virtual std::string suivant() = 0;
};
