#pragma once

#include "IIterator.h"
#include <vector>
#include <string>

class CollectionLivres {
private:
    std::vector<std::string> livres;

    class IteratorLivres : public IIterator {
    private:
        CollectionLivres& collection;
        size_t position;
    public:
        IteratorLivres(CollectionLivres& collection);
        bool hasNext() override;
        std::string suivant() override;
    };

public:
    CollectionLivres();
    void ajouterLivre(const std::string& titre);
    IIterator* creerIterator();
};
