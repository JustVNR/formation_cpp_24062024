#include "stdafx.h"
#include "CollectionLivres.h"

CollectionLivres::CollectionLivres() : livres() {}

void CollectionLivres::ajouterLivre(const std::string& titre) {
    livres.push_back(titre);
}

IIterator* CollectionLivres::creerIterator() {
    return new IteratorLivres(*this);
}

CollectionLivres::IteratorLivres::IteratorLivres(CollectionLivres& collection) : collection(collection), position(0) {}

bool CollectionLivres::IteratorLivres::hasNext() {
    return position < collection.livres.size();
}

std::string CollectionLivres::IteratorLivres::suivant() {
    if (hasNext()) {
        return collection.livres[position++];
    }
    return "";
}
