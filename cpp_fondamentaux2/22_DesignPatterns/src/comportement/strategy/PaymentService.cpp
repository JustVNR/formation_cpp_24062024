#include "stdafx.h"
#include "PaymentService.h"
#include <iostream>

PaymentService::PaymentService(IPaymentStrategy* paymentStrategy) : paymentStrategy(paymentStrategy) {}

PaymentService::~PaymentService() {
    delete paymentStrategy;
}

void PaymentService::makePayment(double amount) {
    paymentStrategy->pay(amount);
}

void PaymentService::setPaymentStrategy(IPaymentStrategy* paymentStrategy) {
    delete this->paymentStrategy;
    this->paymentStrategy = paymentStrategy;
}
