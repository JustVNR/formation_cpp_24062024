#pragma once
#include "IPaymentStrategy.h"

class StripeStrategy : public IPaymentStrategy {
public:
    void pay(double amount) const override;
};
