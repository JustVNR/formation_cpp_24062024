#pragma once
#include "IPaymentStrategy.h"

class IPaymentService {
public:
    virtual ~IPaymentService() = default;
    virtual void makePayment(double amount) = 0;
    virtual void setPaymentStrategy(IPaymentStrategy* paymentStrategy) = 0;
};
