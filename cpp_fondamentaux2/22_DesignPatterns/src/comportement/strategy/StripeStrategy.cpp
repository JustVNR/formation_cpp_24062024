#include "stdafx.h"
#include "StripeStrategy.h"
#include <iostream>

void StripeStrategy::pay(double amount) const {
    std::cout << "Paiement par Stripe de " << amount << " euros." << std::endl;
}
