#pragma once
#include "IPaymentStrategy.h"

class CreditCardStrategy : public IPaymentStrategy {
public:
    void pay(double amount) const override;
};
