#pragma once

class IPaymentStrategy {
public:
    virtual ~IPaymentStrategy() = default;
    virtual void pay(double amount) const = 0;
};
