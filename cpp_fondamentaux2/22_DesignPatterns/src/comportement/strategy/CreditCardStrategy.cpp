#include "stdafx.h"
#include "CreditCardStrategy.h"
#include <iostream>

void CreditCardStrategy::pay(double amount) const {
    std::cout << "Paiement par carte bancaire de " << amount << " euros." << std::endl;
}
