#pragma once
#include "IPaymentService.h"
#include "IPaymentStrategy.h"

class PaymentService : public IPaymentService {
private:
    IPaymentStrategy* paymentStrategy;

public:
    PaymentService(IPaymentStrategy* paymentStrategy);
    virtual ~PaymentService();
    void makePayment(double amount) override;
    void setPaymentStrategy(IPaymentStrategy* paymentStrategy) override;
};
