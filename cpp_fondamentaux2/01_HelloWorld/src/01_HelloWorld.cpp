// 01_HelloWorld.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "stdafx.h"

//#include <iostream>
//#include "windows.h"

/*
* #define : MACRO : directive du pr�processeur permettant de triuver et remplacer une chaine de carat�res par une autre
*/
#define COUT_HELLO_WORLD std::cout << "Hello World!\n"

#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(X) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),X)

#define COUT_COLOR(X, Y){SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),Y); \
std::cout << X << std::endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);}

// #define _DEBUG 0

#if _DEBUG
#define DEBUG_COLOR(X, Y){SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),Y); \
std::cout << X << std::endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);}
#else
#define DEBUG_COLOR(X, Y){}
#endif

#define GREEN	10
#define CYAN	11
#define RED		12
#define MAGENTA	13
#define YELLOW	14
#define WHITE	15

// constexpr introduit en c++ 11 pour �valeur des expressions � la compilation : int�r�t fortement typ�
constexpr int WHITE2 = 15;


int main()
{
    // cout : console out : flux de sortie de la console
    // std : namespace (espace de noms)
    // :: : op�rateur de r�solution de port�e
    // << : op�rateur de redirection de flux

    std::cout << "Hello World!\n";

    COUT_HELLO_WORLD;

    COUT("Hello World!\n");

    TEXT_COLOR(GREEN);

    COUT("Hello World!\n");

    COUT_COLOR("Hello World!", MAGENTA)
    COUT_COLOR("Hello World!", YELLOW)
    DEBUG_COLOR("Hello World!", RED)
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */
