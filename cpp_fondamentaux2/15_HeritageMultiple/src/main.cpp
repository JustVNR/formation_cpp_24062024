#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

class Ovipare {
protected:
    unsigned int _nOeufs;

public : 
    Ovipare(unsigned int nOeufs) : _nOeufs(nOeufs) {}

    void afficher() const
    {
        COUT("Nombre d'oeufs : " << _nOeufs);
    }
};

class Vivipare {
protected:
    unsigned int _gestation;

public:
    Vivipare(unsigned int gestation) : _gestation(gestation) {}

    void afficher() const
    {
        COUT("Dur�e de gestation : " << _gestation);
    }
};

/*
* Comme pour l'h�ritage simple, l'initialisation des attributs h�rit�s doit �tre faire par invocation des constructeurs des classes m�res.
* 
* MAIS l'ex�cution des constructeurs des classes m�re se fait : 
* - SELON L'ORDRE DE DECLARATION D'HERITAGE
* - et NON PAS selon l'odre d'appels des constructeurs dans celui de la classe fille.
*/
class Ovovivipare : public Ovipare, public Vivipare
{
protected :
    bool _especeProtegee;

public:
    Ovovivipare(unsigned int nOeufs, unsigned int gestation, bool protege)
        : Vivipare(gestation), Ovipare(nOeufs), _especeProtegee(protege) {}

    virtual ~Ovovivipare() {};

    // M�thode 2 : Choisir laquel des m�thodes utiliser
    // using Vivipare::afficher;

    // M�thode 3 : red�finir la m�thode
    void afficher() const
    {
        Vivipare::afficher();
        Ovipare::afficher();
    }
    /*
    ATTENTION : le constructeur de Ovipare sera appel� avant celui de Vivipare m�me si le constructeur de ViVipare est appel� en premier... Car c'est l'odre de d�claration d'h�ritage qui prime...*/
};


int main()
{
    SetConsoleOutputCP(1252); 

    Ovovivipare o(3, 4, false);

    /*
    * ATTENTION : L'acc�s � o.afficher() provoquera une erreur de compilation 
    */
    //o.afficher();

    // M�thode 1 : D�masquer la /les m�thode(s) des classe m�res : mauvaise pratique car laisse aux utilisateurs de la classe Ovovivipare la responsabilit� de d�terminer comment doit �tre afficher un objet de type Ovovivipare. Cette responsabilit� devrait incomber aux concepteurs de la classe.
    o.Ovipare::afficher();
    o.Vivipare::afficher();

    o.afficher(); // Ok m�thode r�d�finie dans Ovovivipare
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

