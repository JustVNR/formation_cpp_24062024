#include "stdafx.h"
#include <array>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define COUT_COLOR(X, Y) {SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Y); \
std::cout << X; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);}

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;


/*
 *  |_|_|_|_|_|_|_|
 *  |_|_|_|_|_|_|_|
 *  |_|_|_|_|_|_|_|
 *  |_|_|O|_|_|_|_|
 *  |_|_|X|_|_|_|_|
 *  |_|_|X|O|_|_|_|
 *  |1|2|3|4|5|6|7|
*/

enum Pion { ROUGE, JAUNE, VIDE };
typedef array<array<Pion, 7>, 6> Grille;

static void initialise(Grille& g)
{
	for (auto& line : g)
	{
		for (auto& c : line)
		{
			c = Pion::VIDE;
		}
	}
}

static void affiche(Grille& g)
{
	for (auto& line : g)
	{
		cout << endl << " |";

		for (auto& c : line)
		{
			switch (c)
			{
			case Pion::JAUNE:
				COUT_COLOR('O', YELLOW);
				break;
			case Pion::ROUGE:
				COUT_COLOR('O', RED);
				break;
			case Pion::VIDE:
				COUT_COLOR('_', CYAN);
				break;
			default:
				COUT_COLOR('E', MAGENTA);
				break;
			}
			cout << '|';
		}
	}

	cout << endl << ' ';
	for (size_t i = 0; i < g[0].size(); i++)
	{
		cout << '|' << i + 1;
	}
	cout << '|' << endl << endl;
}

bool jouer(Grille& g, size_t col, Pion p)
{
	if (col >= g[0].size() || g[0][col] != Pion::VIDE) // le num�ro de colonne est invalide
	{
		return false; // le coup est invalide
	}

	// jouer => placer le pion ou il faut
	size_t line = g.size() - 1;

	while (g[line][col] != Pion::VIDE)
		line--;

	g[line][col] = p;

	return true; // le coup est valide
}

static void demandeEtjoue(Grille& grille, Pion joueur)
{
	/*if (joueur == Pion::JAUNE)
	{
		COUT("Joueur jaune : entrez un num�ro de colonne");
	}
	else
	{
		COUT("Joueur rouge : entrez un num�ro de colonne");
	}*/

	COUT("Joueur " << (joueur == Pion::JAUNE ? "jaune" : "rouge") << " : entrez un num�ro de colonne");

	size_t col;
	cin >> col;
	col--; // les indices commencent � 0

	while (not jouer(grille, col, joueur))
	{
		COUT("Coup invalide ! Essaye encore !!");
		cin >> col;
		col--; // les indices commencent � 0
	}
}

int compte(const Grille& g, size_t x, size_t y, int dirX, int dirY)
{
	size_t count = 0;

	size_t newX = x;
	size_t newY = y;

	while (newX < g.size() && newY < g[0].size() && g[x][y] == g[newX][newY])
	{
		count++; // On incr�mente le compteur
		newX += dirX; // Et on d�calle le point adjacent � comparer...
		newY += dirY; // dans la direction d�finie par dirX et dirY
	}
	return count;
}

static bool gagner(Grille& g, Pion joueur)
{
	for (size_t line = 0; line < g.size(); line++)
	{
		for (size_t col = 0; col < g[0].size(); col++)
		{
			if (joueur == g[line][col])
			{
				if (compte(g, line, col, -1, +1) >= 4) return true;
				if (compte(g, line, col, 0, +1) >= 4) return true;
				if (compte(g, line, col, +1, +1) >= 4) return true;
				if (compte(g, line, col, +1, 0) >= 4) return true;
			}
		}
	}
	return false;
}

static bool plein(Grille& grille)
{
	for (size_t i = 0; i < grille[0].size(); i++) {

		if (grille[0][i] == Pion::VIDE)
			return false;
	}
	return true;
}

int main()
{
	SetConsoleOutputCP(1252);

	// grille? 

	//array<array<char, 7>, 6> grille;
	//array<array<Pion, 7>, 6> grille;
	Grille grille;

	// Initialiser la grille
	initialise(grille);
	// Afficher la grille
	affiche(grille);
	// Jouer un coup ? Quand un coup est (in)valide?
	 
	/*while(jouer(grille, 2, Pion::JAUNE))
		affiche(grille);

	COUT("Impossible d'ajouter un pion � cette colonne");*/

	// Jouer tant que quoi ? tant que partie n'est pas fini? Quel(s) crit�re(s) de fin de partie?
	bool gagne = false;
	Pion joueur = Pion::JAUNE;

	do {
		demandeEtjoue(grille, joueur);

		system("cls");

		affiche(grille);

		gagne = gagner(grille, joueur);

		joueur = (joueur == Pion::JAUNE) ? Pion::ROUGE : Pion::JAUNE;

	} while (not gagne and not(plein(grille)));

	if (gagne == true)
	{
		//COUT("joueur " << (joueur == Pion::JAUNE ? "rouge" : "jaune") << " a gagn�");
		cout << "Le joueur ";
		COUT_COLOR("O", joueur == Pion::JAUNE ? RED : YELLOW);
		cout << " a gagn� !\n";
	}
	else
	{
		COUT_COLOR("Match null !!", GREEN);
	}
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

