
#include "stdafx.h"
#include <limits>

#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(X) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),X)

#define COUT_COLOR(X, Y){SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),Y); \
std::cout << X << std::endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);}

#if _DEBUG
#define DEBUG_COLOR(X, Y){SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),Y); \
std::cout << X << std::endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);}
#else
#define DEBUG_COLOR(X, Y){}
#endif

#define GREEN	10
#define CYAN	11
#define RED		12
#define MAGENTA	13
#define YELLOW	14
#define WHITE	15

using namespace std;

/*
* Une variable set � stocker et � acc�der � une d�onn�e en m�moire.
*
* Une variable peut �tre de differents types selon la taille de la m�moire qu'elle permet de stocker.
*
* C++ propose un certain nombre de types primitifs.
*
* Chacun de ces types occupe une certaine taille en m�moire.
*
* 2 compilateurs diff�rents pourront attribuer des tailles dif�rentes � un m�m type.
*
* Pour assurer la portabilit�, le standard c++ d�finit n�anmoins un taille minimum pour chaque type :
*
* char at least 8 bits (1 byte)
	short at least 16 bits
	int at least 16 bits
	long at least 32 bits
	long long (in versions of the language that support it) at least 64 bits
	Each type in the above list is at least as wide as the previous type (but may well be the same).
*
*
* REGLES DE NOMMAGE :
* - uniquement des chiffres et/oudes lettres anis que '_'
* - ne peut pas commencer par un chiffre
* - pas de mot cl� r�serv�
* - sensible � la casse
*/

int k = 45;

int main()
{
	SetConsoleOutputCP(1252);

	bool myBoolNotInitialized; // d�claration d'une variable nomm�e 'myBoolNotInitialized' de type booleen non initialis�e

	// IL FAUT INITIALISER LES VARIABLES AVENT DE S'EN SERVIR

	bool myBool = true;

	bool myBool2(true); // syntaxe alternative

	COUT("sizeof(myBool) = " << sizeof(myBool)); // 1

	char myChar = 'a';

	COUT("sizeof(myChar) = " << sizeof(myChar)); // 1

	myChar = 65; // les caract�res sont stock�s sous forme d'entiers repr�sentant leur position dans la table ASCII

	COUT("myChar = " << myChar); // A

	TEXT_COLOR(GREEN);
	COUT("\n************************************");
	COUT("********** TYPES ENTIERS ***********");
	COUT("************************************\n");
	int myInt = 12;

	myInt = 12 + 1; // calcule 12 + 1 et affecte le r�sultat � la variable myInt

	COUT("sizeof(int) = " << sizeof(int) << ", INT_MIN = " << INT_MIN << ", INT_MAX = " << INT_MAX);
	COUT("sizeof(unsigned int) = " << sizeof(unsigned int) << ", UINT_MAX = " << UINT_MAX);
	COUT("sizeof(short) = " << sizeof(short) << ", SHRT_MIN = " << SHRT_MIN << ", SHRT_MAX = " << SHRT_MAX);
	COUT("sizeof(long) = " << sizeof(long) << ", LONG_MIN = " << LONG_MIN << ", LONG_MAX = " << LONG_MAX);
	COUT("sizeof(long long) = " << sizeof(long long) << ", LLONG_MIN = " << LLONG_MIN << ", LLONG_MAX = " << LLONG_MAX);
	COUT("sizeof(unsigned long long) = " << sizeof(unsigned long long) << ", ULLONG_MAX = " << ULLONG_MAX);


	TEXT_COLOR(YELLOW);
	COUT("\n************************************");
	COUT("********** TYPES FLOTTANTS *********");
	COUT("************************************\n");

	/*
	* Il y a 3 types flottants en c++ :
	* - float
	* - double
	* - long double
	*/

	float myFloat = 1.1f;

	double myDouble = 1.1;

	myDouble = 1.1e3; // notation scientifique

	COUT("sizeof(float) = " << sizeof(float) << ", FLT_MIN = " << FLT_MIN << ", FLT_MAX = " << FLT_MAX);
	COUT("sizeof(double) = " << sizeof(double) << ", DBL_MIN = " << DBL_MIN << ", DBL_MAX = " << DBL_MAX);
	COUT("sizeof(double) = " << sizeof(double) << ", DBL_TRUE_MIN = " << DBL_TRUE_MIN << ", DBL_MAX = " << DBL_MAX);

	/*
	* DBL_MIN : minimum normalized positiv value
	* DBL_TRUE_MIN : minimum positive value
	*
	* Les valeurs normalis�es offrent une meilleure pr�cision et les d�normalis�es permettent de repr�senter des nombres tr�s petits.
	*/

	double minNegativeValue = -DBL_MAX;

	COUT("sizeof(long double) = " << sizeof(long double) << ", LDBL_MAX = " << LDBL_MAX);

	// int a, b;

	//cout << "Entrer la premi�re valeur :";
	//cin >> a; // redirection dans la variable b du user input

	//cout << "Entrer la seconde valeur :";
	//cin >> b;


	int a = 45;
	int b = 54;

	// Intervertir les 2 valeurs

	COUT("a = " << a << ", b = " << b);

	int temp = a;

	a = b;

	b = temp;

	COUT("a = " << a << ", b = " << b);

	// Solution alternative

	a = a + b;
	b = a - b;
	a = a - b;

	COUT("a = " << a << ", b = " << b);

	TEXT_COLOR(GREEN);
	COUT("\n************************************");
	COUT("******** CONVERSION DE TYPES *******");
	COUT("************************************\n");

	int myInt2 = 2;

	long myLong = myInt; // Conversion implicite car "long" est stock� sur davantage d'octets que "int"

	myInt2 = (int)myLong; // Conversion explicite car "int" est stock� sur moins d'octets que "long"

	TEXT_COLOR(GREEN);
	COUT("\n************************************");
	COUT("******** PORTE DES VARIABLES *******");
	COUT("************************************\n");

	/*
	* Une variable n'existe que dans le bloc d'instructions (et ses sous blocs) dans lequel elle est d�clar�e
	* Une instruction se termine par ";"
	* Un bloc d'instruction peut contenir un nombre quelconque d'instruction entre "{}"
	* Une variable d�finie en dehors de tout bloc d'instructions est dite "globale"
	*/

	int i = 0;

	{
		COUT(i); // 0

		int j = 2;

		int i = 3; // Attention! Il est possible de d�finir une variable portant le m�me nom qu'une autre d�finie dans un bloc englobant.
		// Auquel cas la variable nouvellement d�clar�e masque celle d�clar�e dans le bloc englobant

		COUT(i); // 3

		int k = 12; // Une variable globale 'k = 45' est d�j� d�clar�e � l'ext�rieur de la fonction 'main' (mauvaise pratique)

		COUT(k); // 12

		// utilisation de l'op�rateur de r�solution de port�e pour d�masquer la variable globale
		::k = k + ::k;

		COUT(::k); // 57
	}

	COUT(i); // 0

	COUT(k); // 45 + 12 = 57

	TEXT_COLOR(YELLOW);
	COUT("\n************************************");
	COUT("************ CONSTANTES ************");
	COUT("************************************\n");

	const double lightSpeed = 300000;

	// lightSpeed = 300001; erreur une constante est non modifiable

	TEXT_COLOR(WHITE);

}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

