#include "stdafx.h"
#include "Carre.h"
#include "Voiture.h"
#include "Moto.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* L'h�ritage est une relation de type "est un" (un chat est un animal) et non pas une relation "a/poss�de" (aggr�gation)
* 
* Il permet de cfr�er des classes sp�cialis�es (sous classes / classes filles) � partir de classes plus g�n�rales (super class / classe m�re)
* 
* Les classes filles h�ritent : 
* - des attributs
* - des m�thodes
* - du type de la classe m�re
* 
* Les m�thodes h�rit�es pourront �galement �tre red�finies (spacialisation)
* Auquel elles masqueront leur �quivalent dans la classe m�re.
* Les masqu�s pouvant �tre accessibles via l'op�rateur de r�soltion de port�e '::'
* 
* Des attributs et m�thodes suppl�mentaires pourront dans un second temps �tre red�finis dans les classes filles (enrichissement)
* 
* Par transivit� les sous-classes h�ritent des attributs et m�thodes de l'ensemble des classes de leur ascendance.
* 
* ex : animal => chien => labrador
* 
* MAIS ne pourront y acc�der que si leur niveau d'accessibilit� est "public" soit "protected"
* 
* Concernant les cnstructeurs : 
* 
* Chaque constructeur d'une sous classe doit appeler un constructeur de la super classe.
* Cet appel doit �tre le premier des appels, i. e. les attributs propres � la sous classe doivent �tre initialis�s apr�s.
* 
* En cas de non appel explicite � un constructeur de la classe m�re, le constructeur par d�faut sera appel�
* En cas d'absence de constructeur par d�faut => erreur
* 
* Concerant les types d'h�ritage : 
* - public : les membres "public / proteted" de la classe m�re demeurent "public / proteted" dans les classes filles
* - protected : les membres "public / protected" de la classe m�re sont "protected" dans les classes filles
* - private : les membres "public / protected" de la classe m�re sont "private" dans les classe filles
* 
* Le mot cl� "final" permet d'interdire l'h�ritage
*/

class Sterile final {};
// class Fille : public Sterile{}; Erreur : une classe "final" ne peut pas �tre h�rit�

int main()
{
    // SetConsoleOutputCP(1252); 

    Shape shape(2.3, 3.0);

    COUT(shape);

    Rectangle rectangle(3.0, 4.0, 5.5, 12.6); // supprimer #include "windows.h" dans stdafx.h pour �viter conflit de nommage

    COUT(rectangle);

    rectangle.move(2.0, 8.2);

    COUT(rectangle);

    Carre carre(2.0, 3.0, 12.0);

    COUT(carre);
    Carre carre2(carre); // constructeur de copie

    carre2.move(2.0, 8.2);

    COUT(carre2);

	COUT("\n----------------- EXO VEHICULE ---------------------------\n");

	// ------------ EXO -------------

	/*
	* D�finir une classe 'Vehicule' comprenant :
	* - Un constructeur prenant la marque du v�hicule comme argument.
	* - Une fonction afficherMarque() qui affiche la marque du v�hicule.
	*
	* Ajouter 2 classes de Vehicule : Voiture et Moto.
	*
	* Ajouter � chaque classe :
	* - Un constructeur prenant en plus du param�tre de la marque, les param�tres sp�cifiques � chaque type de v�hicule
	*	- nombre de portes pour la voiture
	*	- type de moto pour la moto
	* - Une fonction afficherDetails() qui affiche la marque et les d�tails sp�cifiques de chaque type de v�hicule.
	*
	* Cr�er une voiture et une moto et afficher leurs d�tails.
	*/

	Voiture voiture("Toyota", 4);

	voiture.afficherDetails();

	Moto moto("Honda", "Sportive");

	moto.afficherDetails();

}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

