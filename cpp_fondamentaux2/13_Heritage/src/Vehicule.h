#pragma once
#include "stdafx.h"
class Vehicule
{
	std::string _marque;

public:
	Vehicule(const std::string& marque) : _marque(marque){}

	std::string marque() const
	{
		return _marque;
	}

	/*void afficherMarque() const
	{
		std::cout << "Marque :" << _marque << std::endl;
	}*/

	void afficherMarque() const;
	
};

// A mettre dans un .cpp
void Vehicule::afficherMarque() const
{
	std::cout << "Marque :" << marque() << std::endl;
}