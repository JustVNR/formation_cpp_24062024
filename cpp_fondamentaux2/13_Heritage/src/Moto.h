#pragma once
#include "stdafx.h"
#include "vehicule.h"

class Moto : public Vehicule
{
	std::string _type;

public : 
	Moto(const std::string& marque, const std::string& type) : Vehicule(marque), _type(type){}

	void afficherDetails() const
	{
		Vehicule::afficherMarque();
		std::cout << "Type : " << _type << std::endl;
	}
};