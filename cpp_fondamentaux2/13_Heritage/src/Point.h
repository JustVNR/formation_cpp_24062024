#pragma once
class Point
{
private:
	double _x;
	double _y;

public:

	Point(double x, double y) : _x(x), _y(y) {}

	double x() const
	{
		return _x;
	}

	void x(double x)
	{
		_x = x;
	}

	double y() const
	{
		return _y;
	}

	void y(double y)
	{
		_y = y;
	}
};