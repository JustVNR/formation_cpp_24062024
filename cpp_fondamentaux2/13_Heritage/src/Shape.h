#pragma once
#include "Point.h"
#include <string>

class Shape
{
//private:
protected:
	Point _position; // Aggr�gation : Une Shape poss�de une position (sous forme de Point)

public:
	// Getter
	Point position() const
	{
		return _position;
	}

	// constructeur

	Shape(double x, double y): _position(x, y){ }

	// M�thodes

	void move(double x, double y);

	std::string toString() const;
};

// A mettre dans un Shape.cpp
void Shape::move(double x, double y)
{
	_position.x(_position.x() + x);
	_position.y(_position.y() + y);
}

static std::ostream& operator<<(std::ostream& sortie, Shape const& obj)
{
	return sortie << obj.toString();
}

std::string Shape::toString() const
{
	return "(" + std::to_string(position().x()) + ", " + std::to_string(position().y()) + ")";
}