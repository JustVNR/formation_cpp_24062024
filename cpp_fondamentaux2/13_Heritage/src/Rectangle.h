#pragma once
#include "Shape.h"
#include <string>

// la classe rectangle h�rite publiquement de la clsse Shape

class Rectangle : public Shape
{
private:
	double _largeur;
	double _longueur;

public:
	double largeur() const
	{
		return _largeur;
	}

	double longueur() const
	{
		return _longueur;
	}

	Rectangle(double x, double y, double largeur, double longueur)
		: Shape(x, y), _largeur(largeur), _longueur(longueur){} // Appel au constructeur de la classe m�re pour initialiser ses attributs

	std::string toString() const;
};

// A mettre dans un .cpp

std::string Rectangle::toString() const
{
	return "Position : " + Shape::toString() + " dimensions : (" + std::to_string(largeur()) + " , " + std::to_string(longueur()) + ")";
}

static std::ostream& operator<<(std::ostream& sortie, Rectangle const& obj)
{
	return sortie << obj.toString();
}
