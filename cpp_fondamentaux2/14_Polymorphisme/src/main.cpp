#include "stdafx.h"

#define _USE_MATH_DEFINES 

#include <math.h>
#include <vector>

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;
/*
* Le polymorphisme permet aux instances d'une sous classe, lesquelles sont substituables aux instances des classes de leur ascendance, de conserver leurs propri�t�s (au sens large : m�thodes...) propres.
* 
* La mise en oeuvre du polymorphisme repose sur les concepts :
* - d'h�ritage
* - de r�solution dynamique des liens (par opposition � la r�solution statique des liens)
* 
* - r�solution statique des liens : 
*   - le choix de la m�thode � appeler est effectu� � la compilation
*   - la m�thode appel�e est n�cessairement celle du type de la variable pass�e en param�tre (classe m�re)
* 
* - r�solution dynamique des liens : 
*   - le choix de la m�thode � appeler est effectu� � l'ex�cution
*   - en tenant compte de la nature r�elle de l'instance consid�r�e (classe fille)
* 
* La r�solution dynamique des liens exige :
*   - que la m�thode concern�e soit d�clar�e comme "virtual" dans la classe m�re
*   - qu'elle s'applique � des objets pass�s par r�f�rence ou par pointeur
*/

class Personnage
{
public:

    virtual void afficher() const { COUT("Personnage affich�"); }
    virtual void rencontrer(Personnage& const p) const { COUT("Personnage dit bonjour"); }

    // les m�thodes virtuelles red�finies dans les classes filles h�ritent du caract�re 'virtual' par transitivit�.
};

class Guerrier : public Personnage
{
public:

    void afficher() const { COUT("Guerrier affich�"); }
    void rencontrer(Personnage& const p) const { COUT("Guerrier dit bonjour"); }
};

class Voleur : public Personnage
{
public:

    void afficher() const override { COUT("Voleur affich�"); } // override permet d'�tre certain que la m�thode est bien marqu�e 'virtual' dans la classe m�re
    void rencontrer(Personnage& const p) const  { COUT("Voleur dit bonjour"); }
};

void faire_rencontrer(Personnage& p1, Personnage p2)
{
    // p1 est pass� par r�f�rence et 'rencontrer' est marqu� comme 'virtual' dans la classe m�re 'Personnage' => r�soluton dynamique des liens => comportement polymorphique
    p1.rencontrer(p2);
}

void faire_rencontrer_stat(Personnage p1, Personnage p2)
{
    // p1 n'est ni pass� par r�f�rence ni par pointeur => r�solution statique des liens
    p1.rencontrer(p2);
}

// -----------------------------------------------
// -----------------------------------------------

class Mammifere
{
public:
    Mammifere()
    {
        COUT("Un nouveau mammif�re est n�");
    } // un constructeur ne peut pas �tre 'virtual'

    virtual ~Mammifere()
    {
        COUT("Un mammif�re est mort");
    } // Bonne pratique destructeur virtuel ( pour pouvoir lib�rer les ressources des classes filles)

    void manger() { COUT("Un mammif�re mange."); }

    virtual void bouger() { COUT("Un mammif�re bouge."); }
};

class Dauphin : public Mammifere
{
public:
    Dauphin() { COUT("Un nouveau dauphin est n�"); } 

    ~Dauphin(){ COUT("Un dauphin est mort");}

    void manger() { COUT("Un dauphin mange."); }

    virtual void bouger() { COUT("Un dauphin bouge."); }
};

// ----------------------------------------------------------------
// ------------ MASQUAGE SUBSTITUTION SURCHARGE -------------------
// ----------------------------------------------------------------

/*
* - Surcharge (overloading) : m�me nom de m�thode mais signature diff�rente DANS LA MEME PORTEE
* - Masquage (shadowing) : Entit�s de m�mes nom DANS DES PORTEES DIFFERENTES, la port�e la plus proche masque les port�es les plus lointaines  => une seule m�thode de m�me nom suffit � masquer toutes les m�thodes de la classe m�re ind�pendamment de leur signature
* 
* - Substitution (overriding) : red�finition de m�thodes virtuelles
*   si ovrriding dans une classe fille une m�thode parmi plusieurs surcharg�es dans une clsses m�re, alors toutes les m�thodes (de m�me nom) de la classe m�re seront masqu�es.
*/

class A
{
public:
    virtual void method(int i) const { COUT("A::method(int) : " << i); }

    virtual void method(string const& s) const { COUT("A::method(string) : " << s); }

    // le mot cl� "final" interdit la red�finition
    virtual void methodFinal() const final { COUT(" "); }

    /*
    * Le mot cl� 'final' sera plut�t utilis� sur une m�thode de classe fille qui override une m�thode viturelle de classe m�re, et qui h�rite � ce titre du statut 'virtual' mais qui peut �galement �tre d�clar�e comme 'final' pour que ses propres classes filles ne puissent plus l'overrider � leur tour.
    */
};


class B : public A
{
public:

    /*virtual implicite*/ void method(string const& s) const override { COUT("B::method(string) : " << s); }

    // le mot cl� "final" interdit la red�finition
    // void methodFinal() const { COUT(" "); } ERREUR : impossible de red�finir une m�thode 'final'
   
    /*
    * La classe B n'override que 'method(string const& s)'
    * => La m�thode 'void method(int i)' de la classe A est masqu�e depuis la classe B
    */
};

class C : public A
{
public:
    // nouvelle signature : les m�thodes des classes m�res sont toutes masqu�es
    virtual void method(double d) const { COUT("C::method(double) : " << d); }
};

// --------------------------------------------------------
// ------------------ CLASSES ABSTRAITES ------------------
// --------------------------------------------------------

/*
* Une classe abstraite est une classe contenant au moins une m�thode virtuelle pure
* Une classe abstraite ne peut pas �tre instanci�e (mais peut contenir des constructeurs pour les classses filles)
* Ses sous classes restent elles-m�mes abstraites tant qu'elles n'impl�mentent pas TOUTES les m�thodes virtuelles pures de la classe m�re.
*/

class Shape
{
public:
    Shape() { COUT("constructeur de shape\n"); }

    virtual double perimetre() const = 0;

    virtual double surface() const = 0;
};

class Cercle : public Shape
{
private:
    double _r;

public:
    Cercle (double r) : _r(r) {}

    double perimetre() const override
    {
        return M_PI * _r * 2;
    }

    double surface() const override
    {
        return M_PI * _r * _r;
    }
};

class Carre : public Shape
{
private:
    double _c;

public:
    Carre(double c) : _c(c) {}

    double perimetre() const override
    {
        return _c * 4;
    }

    double surface() const override
    {
        return _c * _c;
    }
};

// --------------------------------------------------------
// -------------- COLLECTIONS HETEROGENES -----------------
// --------------------------------------------------------

class Jeu
{
private:
    // vector<Personnage> personnages; // la collection peut comprendre des personnages de types diff�rents
                                    // MAIS la collection ne permet pas de comportement polymorphique de ses �l�ments car il ne s'agit ni d'une collection de pointeur ni d'une collection de r�f�rences

    vector<Personnage*> personnages; // Collection de pointeurs => possibilit� de comportement polymorphique de ses �l�ments

public:
    void afficher() const;
    void ajouterPersonnage(Personnage* nouveau);
    void clearPersonnages();
};

void Jeu::ajouterPersonnage(Personnage* nouveau)
{
    if (nouveau != nullptr)
    {
        personnages.push_back(nouveau);
    }
}

void Jeu::afficher() const
{
    for (auto const& quidam : personnages)
    {
        quidam->afficher(); // quidam est appel� par r�f�rence et 'afficher' est 'virtual' dans 'Personnage' => comportement polymorphique
    }
}

void Jeu::clearPersonnages()
{
    for (auto quidam : personnages)
    {
        delete quidam;
    }
    personnages.clear();
}

int main()
{
    SetConsoleOutputCP(1252);

    Guerrier g;
    Voleur v;

    faire_rencontrer(g, v);
    faire_rencontrer(v, g);

    // -----------------
    TEXT_COLOR(GREEN);
    COUT("");
    Mammifere* dauphin = new Dauphin(); // Le constructeur de Dauphin appelle d'abord celui de Mammifere

    //(*dauphin).bouger();
    dauphin->bouger(); // 'bouger' est marqu�e comme 'virtual' dans la classe m�re "Mammiferre' ET la variable 'dauphin' est un pointeur => r�solution dynamique des liens => polymorphisme

    dauphin->manger(); // 'manger' n'est pas d�clar�e comme 'virtual' dans la classe m�re 'Mammifere' => r�solution statique des lien = > pas de comportement polymorphique

    delete dauphin; // destructeur virtuel : r�solution dynamique des liens : ~Dauphin appelle ~Mammifere

    TEXT_COLOR(YELLOW);
    COUT("------------------------ MASQUAGE, SUBSTITUTION, SURCHARGE-------------------------");

    B b;

    b.method("2");
    //b.method(2); // Erreur : la m�thode de la classe m�re est masqu�e

    b.A::method(2); // OK : Pour y acc�der il faut la d�masquer gr�ce � l'op�rateur de r�solution de port�e en appelant explicitement la m�thodes de la classe A

    C c;
    c.method(2); // C::method(double)

    c.A::method(2); // A::method(int)
    c.A::method("2"); // A::method(string) 

    A* pa = nullptr;
    pa = &b;
    pa->method("2"); // B::method(string) car 'method' est appel�e via pointeur et est marqu�e comme virtual dans la classe 'A'

    pa->method(2); // A::method(int) car 'method(int)' non red�fini dans classe 'B' 


    pa = &c;
    pa->method("2"); // A::method(string) car 'method(string)' non red�fini dans classe 'C' 
    pa->method(2.1); // !!!!!!!!!!!!!!!!!!! A::method(int) : 2 le 'double' est converti en 'int'

    // pa->C::method(2.1); // ERREUR : A n'h�rite pas de 'C' => Pour pouvoir utiliser la m�thode 'method(double)' de 'C' il ne faut pas passer par un pointeur sur 'A'

    TEXT_COLOR(GREEN);
    COUT("------------------------ CLASSES ABSTRAITES-------------------------");

    // Shape shape; ERREUR : impossible d'instancier une classe abstraite

    Cercle cercle(2.0);
    Carre carre(2.0);

    COUT("carre.perimetre() = " << carre.perimetre());

    TEXT_COLOR(YELLOW);
    COUT("------------------------ COLLECTION HETEROGENE-------------------------");

    Jeu jeu;

    jeu.ajouterPersonnage(new Guerrier());
    jeu.ajouterPersonnage(new Guerrier());
    jeu.ajouterPersonnage(new Voleur());

    jeu.afficher();

    jeu.clearPersonnages();

    TEXT_COLOR(WHITE);
}
/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

