#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
*
* conventions de notation :
*
* - camelCase : maVariable
* - PascaCase : MaVariable
* - snake_case : ma_variable
*/

/// <summary>
/// Fonction retournant le produit de 2 entiers
/// </summary>
/// <param name="a">le premier entier</param>
/// <param name="b">le second entier</param>
/// <returns>le produit des 2 entiers</returns>
static int multiply(int a, int b)
{
	return a * b;
}

// surcharge de la fonction 'multiply'
static int multiply(int a, int b, int c)
{
	return a * b * c;
}

// valeur par d�faut du param�tre 'b'
static int sum(int a, int b = 4)
{
	return a + b;
}

static int sum(double a, double b = 4.5)
{
	return a + b;
}

// Une fonction peut �tre d�finie apr�s avoir �t� appel�e MAIS son prototype doit �tre d�clar� AVANT appel
static void multiplyAndLog(int a, int b);

static double celciusToFahrenheit(double c)
{
	return c * 9.0 / 5.0 + 32.0;
}

static unsigned long factorielle(int n)
{
	if (n == 0 || n == 1) return 1;

	return n * factorielle(n - 1);
}

// les param�tres sont pass�s par valeurs : ici n est une copie de la variable pass�e � la fonction lors de l'appel.
static void increment(int n)
{
	n = n + 1;
	COUT(n);
}

// passage de param�tre par r�f�rence
static void incrementRef(int& n)
{
	n = n + 1;
}

// le passage par r�f�rence permet de "retourner" plusieurs r�sultats � la fois
static void multiplyAndSum(int a, int b, int& mult, int& sum)
{
	mult = a * b;
	sum = a + b;
}

static void swap(int& a, int& b)
{
	int temp = b;
	b = a;
	a = temp;
}

/*
* inline : le compilateur va directement remplacer le bloc d'instructions associ� � la fonction aux endroits ou la fonction est appel�e.
* Cela �vite de manipuler la m�moire comme c'est le cas lors de l'appel de fonctions normales (non inline).
* Utilser le mot clef "inline" est pertinent dans le cas de fonctions simples (avec peu d'instructions) et appel�es souvent.
*
* Dans le cadre de vrais projets, une fonction 'inline' est d�clar�e ET impl�ment�e dans un header (.h)
*
* Les fonctions 'inline' pr�sentent l'avantage, par rapport aux 'macro � la C' de pouvoir typer les param�tres.
*/
static inline void inlineSwap(int& a, int& b)
{
	int temp = b;
	b = a;
	a = temp;
}

static int fibonacci(int n)
{
	if (n == 0) return 0;

	if (n == 1) return 1;

	return fibonacci(n - 1) + fibonacci(n - 2);
}

int main()
{
	SetConsoleOutputCP(1252);

	int result = multiply(2, 3);

	COUT("multiply(2, 3) = " << result);
	COUT("multiply(2, 3, 4) = " << multiply(2, 3, 4));

	COUT("sum(2) = " << sum(2)); // 6
	COUT("sum(2, 6) = " << sum(2, 6)); // 8

	COUT("sum(2.5) = " << sum(2.5)); // 7

	multiplyAndLog(2, 3);

	// Exo : Fonction qui convertit une temp�rature en celcius en Fahrenheit ( F = C * 9/5 + 32)

	double fahrenheit = celciusToFahrenheit(31);

	COUT("celciusToFahrenheit(31) = " << fahrenheit);

	COUT("\n********* FACTORIELLE ***********\n");

	COUT("factorielle(25) = " << factorielle(25));

	COUT("\n********* INCREMENT ***********\n");

	int n = 0;

	COUT("Avant appel de 'increment' par valeur = " << n); // 0
	increment(n);
	COUT("Apr�s appel de 'increment' par valeur = " << n); // 0 // n n'a pas �t� incr�ment�

	COUT("Avant appel de 'increment' par r�f�rence = " << n); // 0
	incrementRef(n);
	COUT("Apr�s appel de 'increment' par r�f�rence = " << n); // 1 // n a �t� incr�ment�

	int mult, sum;

	multiplyAndSum(2, 3, mult, sum);

	COUT("mult = " << mult << ", sum = " << sum);
	// Exo : Impl�menter une fonciton qui �change les valeurs de 2 nombres

	COUT("\n********* SWAP ***********\n");

	swap(sum, mult);

	COUT("mult = " << mult << ", sum = " << sum);

	inlineSwap(sum, mult);

	COUT("mult = " << mult << ", sum = " << sum);

	COUT("\n********* FIBONACCI ***********\n");
	// Afficher les 15 premiers termes de la suite de Fibonacci

	for (int i = 0; i < 15; i++)
	{
		COUT("Fibonacci(" << i << ") = " << fibonacci(i));
	}
}



void multiplyAndLog(int a, int b)
{
	COUT(a * b);
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

