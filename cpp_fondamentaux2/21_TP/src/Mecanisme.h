#pragma once
#include "Produit.h"
#include <iostream>

class Mecanisme : public Produit
{

private:

	std::string _heure;

protected:

	// Virtuelle pure <=> les classe filles DEVRONT impl�menter "afficherType"
	// Virtuelle pure <=> classe Abstraite
	virtual void afficherType(std::ostream& sortie) const = 0;

	// On veut pouvoir offrir la valeur par d�faut aux sous-classes : par d�faut on affiche juste l'heure
	virtual void afficherCadran(std::ostream& sortie) const {
		sortie << _heure << " ";
	}

public:

	Mecanisme(double prixDeBase, std::string heure = "12:00") : Produit(prixDeBase), _heure(heure) {}

	// "final" empeche la red�finition dans les sous-classes
	// => Tous les m�canismes DOIVENT s'afficher comme ceci () :
	virtual void afficher(std::ostream& sortie) const override final
	{
		sortie << "Mecanisme : "; afficherType(sortie);  // MAIS les sous-classes DEVRONT red�finir "afficherType" (car virtuelle pure)
		sortie << " - Cadran : "; afficherCadran(sortie); // Et POURRONT red�finir "afficherCadran"
		sortie << " - Prix : "; Produit::afficher(sortie);
	}

	// copie polymorphique
	virtual std::unique_ptr<Mecanisme> copie() const = 0; // M�thode virtuelle pure

	virtual void mettre_a_l_heure(std::string nouvelle) {
		_heure = nouvelle;
	}
};

class MecanismeAnalogique : public virtual Mecanisme
{
private:

	int _date;

protected:

	virtual void afficherType(std::ostream& sortie) const override
	{
		sortie << "analogique";
	}

	virtual void afficherCadran(std::ostream& sortie) const override
	{
		// Affichage de l'heure de base par d�masquage de la m�thode de la classe m�re "Mecanisme"
		Mecanisme::afficherCadran(sortie);

		// et de la date
		sortie << " - date : " << _date << " ";
	}

public:

	MecanismeAnalogique(double prixDeBase, std::string heure, int date) : Mecanisme(prixDeBase, heure/* = "12:00" NOK*/), _date(date) {} // "date" n'a pas de valeur par d�faut => "heure", situ� avant, ne peut pas en avoir non plus
	MecanismeAnalogique(double prixDeBase, int date) : Mecanisme(prixDeBase), _date(date) {} // pour b�n�ficier de l'heure par d�faut de la classe m�re "Mecanisme"

	// copie polymorhique
	virtual std::unique_ptr<Mecanisme> copie() const override
	{
		return (std::unique_ptr<Mecanisme>) new MecanismeAnalogique(*this); // *this => contenu point� par this <=> contenu de l'instance courante
	}
};

class MecanismeDigital : public virtual Mecanisme
{
private:

	std::string _reveil;

protected:

	virtual void afficherType(std::ostream& sortie) const override
	{
		sortie << "digital";
	}

	virtual void afficherCadran(std::ostream& sortie) const override
	{
		// Affichage de l'heure de base
		Mecanisme::afficherCadran(sortie);

		// et de la date
		sortie << " - reveil : " << _reveil << " ";
	}

public:

	MecanismeDigital(double prixDeBase, std::string heure, std::string reveil) : Mecanisme(prixDeBase, heure), _reveil(reveil) {}
	MecanismeDigital(double prixDeBase, std::string reveil) : Mecanisme(prixDeBase), _reveil(reveil) {}

	// copie polymorhique
	virtual std::unique_ptr<Mecanisme> copie() const override
	{
		return (std::unique_ptr<Mecanisme>) new MecanismeDigital(*this); // *this => contenu point� par this <=> contenu de l'instance courante
	}
};

class MecanismeHybride : public MecanismeAnalogique, MecanismeDigital
{
protected:

	virtual void afficherType(std::ostream& sortie) const override
	{
		sortie << "hybride\n";
	}

	virtual void afficherCadran(std::ostream& sortie) const override
	{
		sortie << std::endl;
		sortie << "\t - Analogique : ";
		MecanismeAnalogique::afficherCadran(sortie);
		sortie << std::endl;
		sortie << "\t - Digital : ";
		MecanismeDigital::afficherCadran(sortie);
		sortie << std::endl;
	}

public:

	MecanismeHybride(double prixDeBase, std::string heure, int date, std::string reveil)
		: Mecanisme(prixDeBase, heure),
		MecanismeAnalogique(prixDeBase, heure, date),
		MecanismeDigital(prixDeBase, heure, reveil) {}

	MecanismeHybride(double prixDeBase, int date, std::string reveil)
		: Mecanisme(prixDeBase),
		MecanismeAnalogique(prixDeBase, date),
		MecanismeDigital(prixDeBase, reveil) {}


	// copie polymorhique
	virtual std::unique_ptr<Mecanisme> copie() const override
	{
		return (std::unique_ptr<Mecanisme>) new MecanismeHybride(*this); // *this => contenu point� par this <=> contenu de l'instance courante
	}
};