#pragma once
#include "Produit.h"
#include "Mecanisme.h"
#include "Accessoire.h"

#include <vector>
#include <memory>

class Montre : public Produit
{
private:
	std::vector<std::unique_ptr<Accessoire>> _accessoires; // idem
	std::unique_ptr<Mecanisme> _coeur; // on passe par pointeur pour pouvoir utiliser le polymorphisme

	// Montre(const Montre&) = delete; // constructeur de copie (surface ou profonde) � red�finir � cause des pointeurs (pour le moment prohib�)
	// Montre& operator=(Montre&) = delete; //op�rateur d'affectation provisoirement prohib�

public:

	Montre() = default; // On r� ajoute le constucteur par d�faut car il a saut� � cause du constructeur de copie

	Montre(Mecanisme* meca) : _coeur(meca) { }

	virtual ~Montre() {}

	virtual double prix() const override
	{
		double result = Produit::prix();

		for (auto const& pA : _accessoires) // Pour boucler sur des uniquePtr il faut passer par des r�f�rences
		{
			result += pA->prix();
		}

		if (_coeur)
		{
			result += _coeur->prix();
		}

		return result;
	}

	virtual void afficher(std::ostream& sortie) const override
	{
		sortie << "Accessoires :" << std::endl;

		for (auto const& pA : _accessoires) // Pour boucler sur des uniquePtr il faut passer par des r�f�rences
		{
			sortie << "- " << *pA << std::endl; // On a surcharg� l'op�rateur <<
		}

		if (_coeur)
		{
			sortie << std::endl << *_coeur << std::endl;
		}

		sortie << "Prix total = " << prix() << std::endl;
	}

	void operator+=(Accessoire* pAccessoire)
	{
		_accessoires.push_back((std::unique_ptr<Accessoire>) pAccessoire);
	};

	void mettre_a_l_heure(std::string nouvelle)
	{
		_coeur->mettre_a_l_heure(nouvelle);
	}

	// COPIE (profonde) POLYMORPHIQUE // A LA FIN

	// Ne pas oublier d'appeler les constructeurs de copie des super classes (ici uniquement la classe "Produit")

	Montre(Montre const& source) : Produit(source), _coeur(source._coeur->copie())
	{
		for (auto const& pA : source._accessoires) // Pour boucler sur des uniquePtr il faut passer par des r�f�rences
		{
			_accessoires.push_back(pA->copie());
		}
	}

	Montre& operator=(Montre source) // Passage par valeur => et donc appel au constructeur de copie (profonde)
	{
		swap(_coeur, source._coeur); // remplace coeur par source.coeur (qui est une copie) et r�ciproquement <=> attribue � "_coeur" la valeur de "source.coeur" 
		swap(_accessoires, source._accessoires);

		return *this;
	}
};

