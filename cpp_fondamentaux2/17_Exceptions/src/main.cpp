#include "stdafx.h"
#include <exception>

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define COUT_COLOR(X, Y) {SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Y); \
std::cout << X; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);}

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

class NotEnoughPaperException : public exception {
public:
    const char* what() const
    {
        return "Not Enough Paper";
    }
};

const size_t pageSize = 20;


class Printer
{
private:
    string _nom;

    int _paper;

public:

    Printer(string nom, int pages) : _nom(nom), _paper(pages) {}

    void print(string text)
    {
        size_t requiredPaper = text.length() / pageSize + 1;

        if (requiredPaper > _paper)
            // throw "No enough paper";
            // throw 101;
            // throw out_of_range("No enough paper"); // #include <exception>
            throw NotEnoughPaperException();

        COUT("Print..." << endl << text);

        _paper -= requiredPaper;
    }
};
int main()
{
    SetConsoleOutputCP(1252); 

    Printer printer("HP", 3);

    try
    {
        printer.print("Text to print");
        printer.print("Text to print");
        printer.print("Text to print");
        printer.print("Text to print");
    }
  
    catch (const char* e)
    {
        COUT_COLOR("out_of_range : " << e, RED);
    }
    catch (int exCode)
    {
        COUT_COLOR("out_of_range : " << exCode, RED);
    }
    catch (out_of_range e )
    {
        COUT_COLOR("out_of_range : " << e.what(), RED);
    }
    catch (NotEnoughPaperException e)
    {
        COUT_COLOR(e.what(), RED);
    }
    catch (...)
    {
        COUT_COLOR("Une exception s'est produite", RED);
    }

    // Allocation dynamique

    Printer* pPrinter = new Printer("HP", 3);

    try
    {
        pPrinter->print("Text to print");
        pPrinter->print("Text to print");
        pPrinter->print("Text to print");
        pPrinter->print("Text to print");

        // delete pPrinter; // cette ligne ne sera pas atteinte en cas d'exception plus haut dans le bloc try...
    }
    catch (...)
    {
        COUT("Traitement de l'exception");

        // M�thode 2 : lib�rer la ressource ici et re throw l'exception pour traitement � un niveau plus �lev�
        /*delete pPrinter;
        throw;*/
    }

    // M�thode 1 : lib�rer la ressource ici
    delete pPrinter;
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

