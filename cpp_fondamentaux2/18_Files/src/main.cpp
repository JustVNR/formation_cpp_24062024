#include "stdafx.h"
#include <fstream> // fstream requiert iostream (d�j� pr�sent dans stdafx.h)
#include <string>
#include <sstream>
#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(X) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),X)

#define COUT_COLOR(X, Y) {SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Y); \
std::cout << X << std:: endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);}

#define GREEN	10
#define CYAN	11
#define RED		12
#define MAGENTA	13
#define YELLOW	14
#define WHITE	15

// https://www.w3schools.com/cpp/cpp_files.asp
// https://www.youtube.com/watch?v=EaHFhms_Shw

using namespace std;

int main()
{
	fstream myFile;
	myFile.open("cppfile.txt", ios::out); // ouverture en �criture

	if (myFile.is_open())
	{
		myFile << "Hello World to text file\n";
		myFile << "Hello World to text file\n";
		myFile.close();
	}

	myFile.open("cppfile.txt", ios::app); // ouverture en mode append

	if (myFile.is_open())
	{
		myFile << "Hello World appened to text file\n";
		myFile.close();
	}

	myFile.open("cppfile.txt", ios::in); // ouverture en lecture

	if (myFile.is_open())
	{
		string line;

		while (getline(myFile, line)) // getline dans <string> (� inclure)
		{
			COUT(line);
		}
		myFile.close();
	}

	COUT_COLOR("\n--------------- AJOUT AU DEBUT DU FICHIER ----------------\n", GREEN);

	myFile.open("cppfile.txt", std::ios::in | std::ios::out); // ouverture en mode lecture et �criture

	if (myFile.is_open())
	{
		stringstream existingContent; // #include <sstream>
		existingContent << myFile.rdbuf();

		myFile.seekp(0, std::ios::beg);

		myFile << "Hello World appened to begin of text file\n";
		myFile << existingContent.str();
		myFile.close();
	}

	myFile.open("cppfile.txt", ios::in); // ouverture en lecture

	if (myFile.is_open())
	{
		string line;

		while (getline(myFile, line)) // getline dans <string> (� inclure)
		{
			COUT(line);
		}
		myFile.close();
	}
}
