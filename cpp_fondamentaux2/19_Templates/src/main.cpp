#include "stdafx.h"
#include "string.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

/*
Exemple : Fonction qui retourne le minimium de 2 variavbles, qu'elles soient de type entier, double, float...

pb : il faut surcharger la fonction pour chaque type

Le concept de "Template" permet d'avoter d'avoir � �crir toutes les surcharges possibles.
Le compilateur cr�era la ou lesdites fonctions surcharg�es au besoin � partir du mod�le (template)
Un template n'�tant pas une fonction, il doit donc �tre d�fini dans un .h (et pas un .cpp)

*/

//int mini(const int a, const int b)
//{
//    return a < b ? a : b;
//}
//
//float mini(const float a, const float b)
//{
//    return a < b ? a : b;
//}

template<typename T>
T mini(T a, T b)
{
    return a < b ? a : b;
}

template<typename T>
T& miniRef(const T& a, const T& b)
{
    return (T&) (a < b ? a : b);// on cast le retour en r�f�rence sur type T
}

// Il est possible d'avoir une fonction et un template portant le m�me nom.
// Le compilateur cherchera d'abord une fonction. S'il n'en trouve pas il cherchera un template.

string& miniRef(string& a, string& b)
{
    return a.compare(b) < 0 ? a : b; // compare dans string.h
}

// ------------------------------------------------------
// ----------------- CLASS TEMPLATE ---------------------
// ------------------------------------------------------

template<typename T>
class Array
{
private:
    T* _array;
    size_t _size;

public:
    Array(size_t size = 10) : _array(new T[size]), _size(size)
    {
        for (size_t i = 0; i < _size; i++)
        {
            _array[i] = T(); // T() => valeur par d�faut du type consid�r�
        }
    }

    ~Array() {
        delete[] _array;
    }

    size_t getSize() const
    {
        return _size;
    }

    T& operator[](size_t position) const;

    Array(Array<T> const& source); // construteur de copie

    Array<T>& operator=(Array<T> const& source); // surcharge de l'op�rateur d'affectation
};

template<typename T>
T& Array<T>::operator[](size_t position) const
{
    if (position >= _size)
    {
        // throw "array index out of range";
        throw out_of_range("array index out of range");
    }

    return _array[position];
};

// Surcharge externe
template<typename T>
ostream& operator<<(ostream& sortie, Array<T> const& arr)
{
    sortie << "[ ";

    for (size_t i = 0; i < arr.getSize() - 1; i++)
    {
        sortie << arr[i] << " - ";
    }

    sortie << arr[arr.getSize() - 1] << " ]" << endl;

    return sortie;
};

template<typename T>
Array<T>& Array<T>::operator=(Array<T> const& original)
{
    delete[] _array; // d�sallocation de l'instance en cours

    _array = new T[original._size];

    _size = original._size;

    for (size_t i = 0; i < _size; i++)
    {
        _array[i] = original._array[i];
    }

    return *this;
};

template<typename T>
Array<T>::Array(Array<T> const& original) : _array(new T[original._size]), _size(original._size)
{
    for (size_t i = 0; i < _size; i++)
    {
        _array[i] = original._array[i];
    }
};

int main()
{
    SetConsoleOutputCP(1252); 

    COUT("----------- FUNCTION TEMPLATE --------------");
    int a = 2;
    int b = 3;

    COUT(mini(a, b));

    float af = 2.1f;
    float bf = 3.2f;

    COUT(mini(af, bf));

    string as = "b";
    string bs = "a";

    COUT(mini(as, bs));

    COUT(miniRef(as, bs));

    COUT(miniRef<float>(a, bf)); // 2 param�tres de types diff�rents : on sp�cifie lequel des types le template doit appliquer

    TEXT_COLOR(YELLOW);
    COUT("\n----------------------- CLASSE TEMPLATE -------------------------------\n");

    Array<int> intArray(3);

    COUT(intArray); // [ 0 - 0 - 0 ]

    intArray[1] = 10; // possible car operator[] renvoie une r�f�rence !!

    COUT(intArray); // [ 0 - 10 - 0 ]

    Array<string> stringArray(3);

    COUT(stringArray);
    stringArray[0] = "Riri";
    stringArray[1] = "Fifi";
    stringArray[2] = "Loulou";

    COUT(stringArray);

    TEXT_COLOR(YELLOW);
    COUT("\n----------------------- COPIE -------------------------------\n");

    Array<int> intArray2;

    intArray2 = intArray;

    intArray[0] = 100;

    COUT(intArray); // [100 - 10 - 0]
    COUT(intArray2);// [0 - 10 - 0]

    TEXT_COLOR(WHITE);


}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

