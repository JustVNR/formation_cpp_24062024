#include "stdafx.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

/*
* "struct" permet de cr�er de nouveaux types permettant de :
* - regrouper des donn�es de types potentiellement diff�rentes
* - faire retourner plusieurs valeurs � une fonction
* - simplifier la conception des programmes
*/

struct Date
{
	int jour;
	int mois;
	int annee;
};

struct Personne
{
private:

	int age;

	/*
	* Par d�faut tous les �l�ments d'une structures sont publics.
	* Ce qui enfreint l'un des principes fondamentaux de la P.O.O. : le principe d'encapsulation
	*/

public:
	string nom;

	double taille;

	char sexe;

	// Constructeur : m�thode portant le m�me nom que la structure et ne retournant rien
	// Un constructeur permet d'instancier des objets du type consid�r� (ici le type Personne)
	// Constructeur sans param�tre : implicitement cr�� par d�faut en l'absence de tout autre constructeur
	// Doit �tre explicitement d�clar� au besoin en pr�sence d'autre(s) constructeur(s)
	Personne()
	{
		nom = "Quidam";
		taille = 0.0;
		age = 0;
		sexe = 'M';
	}

	// Un constructeur peut �tre surcharg�
	Personne(string n, double t, int a) // constructeur � 3 param�tres
	{
		nom = n;
		taille = t;
		age = a;
		sexe = 'M';
	}

	Personne(string n, double t, int a, char s) // constructeur � 4 param�tres
	{
		nom = n;
		taille = t;
		age = a;
		sexe = s;
	}

	// Destructeur
	// - n'acc�pte pas de param�tres
	// - ne peut pas �tre surcharg�
	// - ne peut pas �tre appel� explicitement
	// - automatiquement appel� lors de la destruction de l'objet
	~Personne()
	{
		COUT("Une personne est d�truite");
	}

	// M�thodes

	string toString()
	{
		ostringstream oss; // #include <sstream>

		oss << setprecision(3) << taille; // #include <iomanip>

		// #include <string> pour utiliser to_string
		return "nom = " + nom + " - taille = " + oss.str() + " - age = " + to_string(age) + " - sexe = " + sexe;
	}

	void anniversaire()
	{
		age++;
		nbAnniversaires++;
	}

	int getAge() const // const : emp�che de modifier l'objet courant
	{
		// Il est possible ici d'impl�menter une logique m�tier, par exemple ne permettre l'acc�s � la propri�t� qu'� certains profils d'utilisateur

		// age++; // Erreur 'age' n'est pas modifiable car 'getAge()' est marqu�e comme 'const'
		nbAccessGetAge++; // OK car 'nbAccessGetAge' est d�clar� comme 'mutable'
		return age;
	}

	void setAge(int a)
	{
		// Il est possible ici d'impl�menter une logique m�tier, par exemple interdire des ages inf�rieurs � 18...
		age = a;
	}

	// m�thode statique 
	static int getNbAnniverssaires()
	{
		// age--; Erreur : on ne peut pas acc�der � une variable d'instance dans une m�thode statique
		return nbAnniversaires;
	}

private:
	mutable int nbAccessGetAge = 0; // mutable permet aux m�thodes 'const' de modifier la variable

	//static int nbAnniversaire = 0; // Erreur : il n'est pas possible d'initialiser une variable 'static' � l'int�rieur de la classe /structure

	static int nbAnniversaires; // Une variable statique est partag�e par toutes les instances de la structure / classe
};

int Personne::nbAnniversaires = 0; // Initialisation de la variable statique

// typedef permet de cr�er des alias pour les types existants
typedef vector<Personne> Personnes;

// Cr�er une fonction CreatePersonne qui :
// - demande � l'utilisateur de renseigner l'ensemble des param�tres associ�s � un objet de type "Personne"
// - Le sexe renseign� devra �tre soit 'M' ou 'm' soit 'F' ou 'f'
// - retourne l'objet ainsi cr��

Personne createPersonne()
{
	Personne p;

	COUT("Saisie d'une nouvelle personne :");
	cout << "Nom :";
	// cin >> p.nom; // ne r�cup�re qu'un mot
	getline(cin, p.nom); // r�cup�re toute la ligne

	cout << "Taille : ";
	cin >> p.taille;

	int ageTemp;
	cout << "Age : ";
	cin >> ageTemp;

	p.setAge(ageTemp);

	do {
		char sexe;

		COUT("Femme [F] / Homme [M]");

		cin >> sexe;

		p.sexe = toupper(sexe);

	} while (p.sexe != 'M' && p.sexe != 'F');

	return p;
}
int main()
{
	SetConsoleOutputCP(1252);

	Date aujourdhui = { 25, 6, 2024 };

	COUT("Jour = " << aujourdhui.jour << " - mois = " << aujourdhui.mois << " - annee = " << aujourdhui.annee);

	Personne loulou; // Cr�ation d'un objet de type Personne
	COUT(loulou.nom); // 'Quidam'
	// loulou.age = 12; // Erreur : age est d�sormais priv�

	Personne riri("Riri", 0.75, 12, 'M');

	// Personne fifi("Fifi", 0.75); Erreur pas de constructeur � 2 param�tres

	COUT(riri.toString());

	COUT(loulou.toString());
	loulou.setAge(12);
	COUT(loulou.toString());

	COUT(loulou.getAge()); // 12

	COUT("Personne::getNbAnniverssaires() = " << Personne::getNbAnniverssaires()); // 0

	COUT(riri.toString()); // age = 12
	riri.anniversaire();
	COUT(riri.toString()); // age = 13

	COUT("Personne::getNbAnniverssaires() = " << Personne::getNbAnniverssaires()); // 1

	// COUT(Personne::nbAnniversaires); // Erreur : nbAnniversaires est priv�e

	COUT("\n************ VECTEUR DE PERSONNES ***************\n");
	//vector<Personne> personnes = {
	Personnes personnes = {
		Personne("Riri", 0.75, 12, 'M'),
		Personne("Fifi", 0.75, 12, 'M'),
		Personne("Loulou", 0.75, 12, 'M')
	};

	for(auto& p : personnes)
	{
		COUT(p.toString());
	}
	
	COUT("");

	Personne createdPerson = createPersonne();

	COUT(createdPerson.toString());
	// Appel implicite au destructeur pour chaque instance existante
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

