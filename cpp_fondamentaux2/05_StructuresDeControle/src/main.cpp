#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define COUT_COLOR(X, Y){SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),Y); \
std::cout << X << std::endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),15);}

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

int main()
{
    SetConsoleOutputCP(1252); 

	TEXT_COLOR(WHITE);
	COUT("\n***************************************");
	COUT("*************** CONDITIONS ************");
	COUT("***************************************\n");

	int n = 15;

	if (n > 10)
	{
		COUT("n est sup�rieur � 10");
	}
	else if (n < 10)
	{
		COUT("n est inf�rieur � 10");
	}
	else
	{
		COUT("n est �gal � 10");
	}

	// Op�rateur ternaire

	int time = 20;

	// sans op�rateur ternaire
	if (time < 18)
	{
		COUT("bonjour");
	}
	else
	{
		COUT("bonsoir");
	}

	// avec...
	COUT((time < 18 ? "bonjour" : "bonsoir"));

	// Switch

	int note = 10;

	switch (note)
	{
	case 10:
		COUT("note vaut 10");
		break;
	case 14:
		COUT("note vaut 14");
		break;
	default:
		COUT("n ne vaut ni 10 ni 14");
	}

	TEXT_COLOR(GREEN);
	COUT("\n***************************************");
	COUT("***************** BOUCLES **************");
	COUT("***************************************\n");

	COUT("- BOUCLE FOR : \n");
	
	for (int i = 0; i < 5; i++)
	{
		COUT(i);
	}

	COUT("- BOUCLE FOR (EACH) (C++11): \n");

	for (int item : {0, 1, 2, 3, 4})
	{
		COUT(item);
	}

	// Exercice moyenne
	// Demander � l'utilisateur de saisir un nombre de notes (int)
	// Puis de saisir les valeurs de chacune des notes enti�res
	// et de retourner la moyenne
	int nNotes = 0;
	double somme = 0;

	/*cout << "Entrer le nombre de notes :";
	cin >> nNotes;

	if (nNotes > 0)
	{
		for (int i = 1; i <= nNotes; i++)
		{
			cout << "Entrez la note " << i << " : ";
			cin >> note;
			somme += note;
		}
	}

	COUT("La moyenne vaut : " << somme / nNotes);*/

	// Boucles conditionnelles

	/*
	* Boucle While : r�p�tition du bloc d'instructions tant qu'une condition est �valu�e � true
	*/

	int i = 0;

	while (i < 5)
	{
		COUT(i);
		i++;
	}

	while (true)
	{
		COUT(i);
		i++;
		if (i == 10) break;
	}

	// Boucle Do While : idem boucle 'While' sauf que la condition est v�rifi�e APRES l'ex�cution du bloc d'instructions. On est donc certain de passer au moins un fois dans la boucle m�me si la conditon est fausse d�s le d�part
	do
	{
		COUT("Veuillez entrer le nombre de notes");
		cin >> nNotes;
	} while (nNotes <= 0);

	// Exercice: Calculatrice
	// Saisir dans la console :
	// - un nombre � virgule flottante a
	// - un caract�re 'op�rateur' qui aura pour valeur valide "+", "-", "*" ou "/" 
	// - un nombre � virgule flottante b
	// Afficher:
	// - Le r�sultat de l�op�ration
	// - Un message d�erreur si l�op�rateur est incorrect
	// - Un message d�erreur si l�on fait une division par 0

	double a, b;
	char op;

	cout << "Saisissez un double : ";
	cin >> a;

	cout << "Saisissez un op�rateur ( + - * / ) : ";
	cin >> op;

	cout << "Saisissez un double : ";
	cin >> b;

	switch (op)
	{
	case '+':
		COUT(a << " + " << b << " = " << (a + b));
		break;
	case '-':
		COUT(a << " - " << b << " = " << (a - b));
		break;
	case '*':
		COUT(a << " * " << b << " = " << (a * b));
		break;
	case '/':
		if (b == 0.0)
		{
			COUT_COLOR("DIVISION PAR ZERO", RED);
		}
		else
		{
			COUT(a << " / " << b << " = " << (a / b));
		}
		break;
	default:
		COUT_COLOR("'" <<op << "' n'est pas valide", RED);
	}

	TEXT_COLOR(WHITE);
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

